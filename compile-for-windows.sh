#! /bin/bash

if [ ! -d /opt/mxe -a "$1" != "-i" ]; then
    echo "Need to install mxe in /opt. Run this script wwith -i, again."
    echo "Installation of mxe will take around one hour and 1.2Gbyte of /opt."
    echo "http://stackoverflow.com/questions/14170590/building-qt-5-on-linux-for-windows"
    exit 1
elif [ ! -d /opt/mxe ]; then
    pushd /opt
    sudo git clone https://github.com/mxe/mxe.git
    sudo apt-get install cmake qtbase scons yasm
    cd mxe
    sudo make -j10 qtquick1
    echo "Installation ended, now run the script again."
    exit 0
fi

export PATH=/opt/mxe/usr/bin:$PATH
/opt/mxe/usr/i686-pc-mingw32/qt5/bin/qmake
make -j10

ls -lha release/*.exe
