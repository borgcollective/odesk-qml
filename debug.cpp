#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <QString>

#include "debug.h"
#include <time.h>
#include <sys/time.h>


static time_t  start(time(NULL));

/******************************************************************************
 * Backtrace stuff.
 */
#if 0
#include <execinfo.h>

size_t
pip_app_print_trace (
		bool print) 
{
	void         *array[1024];
	size_t        size;
	size_t        i;
	/*
	 * Getting the information.
	 */
	size = backtrace (array, 1024);

	if (print) {
	    char        **strings;
    	strings = backtrace_symbols (array, size);

		SysDebug::sysPrintMsg (
                QtDebugMsg, "ERROR: ",
                "---- Obtained %4zd stack frames. -------", size);
		
        for (i = 0; i < size; i++) {
			SysDebug::sysPrintMsg (
                QtDebugMsg, "ERROR: ","[%03d] %s", i, strings[i]);       
        }

		SysDebug::sysPrintMsg (
                QtDebugMsg, "ERROR: ",
                "---------------------------------------");
	}

	return size;
}

void 
print_stack_prefix(void)
{
	return;

	size_t size;
	unsigned int   n;

	size = pip_app_print_trace(FALSE);
	for (n = 10; n < size; ++n)
		printf(" ");
}

void 
segfault_event_handler (
        int signum) 
{
    Q_UNUSED(signum);

	SYS_WARNING ("---- Segmentation fault----------------");
	pip_app_print_trace(TRUE);
	exit(1); 
}
#endif



// for symbian we have to use qDebug et al
#ifdef Q_OS_SYMBIAN 
void
SysDebug::sysPrintMsg(
        QtMsgType     type,
        const char   *function,
        const char   *formatstring,
        ...)
{    
    struct         timeval tv;
    va_list        args;
    QString        message;
    QString        msg;
    
    va_start(args, formatstring);

    gettimeofday (&tv, NULL);
    message = QString("%1.%2 %3: ").
        arg(tv.tv_sec - start).
        arg(tv.tv_usec).
        arg(function);

    msg.vsprintf(formatstring, args);
    message += msg;

    /*
     * Never give higher than qWarning(), or the damn thing will abort the
     * process.
     */
    switch (type) {
        case QtDebugMsg:
            qDebug ("%s", message.toLatin1().data());
            break;

        case QtWarningMsg:
            qWarning ("%s", message.toLatin1().data());
            break;

        case QtCriticalMsg:
            qWarning ("%s", message.toLatin1().data());
            break;

        case QtFatalMsg:        
            qWarning ("%s", message.toLatin1().data());
            break;
    }
}

void
SysDebug::sysLogMsg (
        const char   *function,
        const char   *file,
        int           line,
        const char   *formatstring,
        ...)
{
    struct     timeval tv;
    va_list    args;
    QString    msg;
    QString    func;

    va_start(args, formatstring);

    gettimeofday (&tv, NULL);
    func = QString("%1.%2 %3() file '%4' line %5").
        arg(tv.tv_sec - start).
        arg(tv.tv_usec).
        arg(function, file).
        arg(line);

    msg.vsprintf(formatstring, args);

    /*
     * Never give higher than qWarning(), or the damn thing will abort the
     * process.
     */
    qDebug ("%s", func.toLatin1().data());
    qDebug ("    %s", msg.toLatin1().data());
}
#else
/*
 * This function is used to print debug and error messages, an enhanced version
 * of the SysDebug::sysMsg(). Please use this function anly through macros (like
 * SYS_DEBUG() for example) so the change in the function interface does not
 * mean the change of all the code calling it).
 */
void
SysDebug::sysPrintMsg (
        QtMsgType     type,
        const char   *function,
        const char   *formatstring,
        ...)
{
    struct         timeval tv;
    FILE          *stream = stdout;
    va_list        args;

    gettimeofday (&tv, NULL);
#ifdef MCB_PROFILE_IOS
    /* This is needed because tv_usec returns '__darwin_suseconds_t'
     * (aka 'int') on iOS */
    fprintf (stream, "%ld.%06d ",
            tv.tv_sec - start, tv.tv_usec);
#else
    fprintf (stream, "%ld.%06ld ",
            tv.tv_sec - start, tv.tv_usec);
#endif //IOS
    va_start (args, formatstring);
    switch (type) {
        case QtDebugMsg:
            fprintf (stream, "%s%s%s: ",
                    TERM_GREEN TERM_BOLD, function, TERM_NORMAL);
            vfprintf (stream, formatstring, args);
            break;

        case QtWarningMsg:
            fprintf (stream, "%s%s%s: ",
                    TERM_YELLOW, function, TERM_NORMAL);
            vfprintf (stream, formatstring, args);
            break;

        case QtCriticalMsg:
            fprintf (stream, "%s%s%s:\n",
                    XTERM_COLOR_14, function, TERM_NORMAL);
            vfprintf (stream, formatstring, args);
            break;
        
        case QtFatalMsg:
            fprintf (stream, "%s%s%s:\n",
                    TERM_GREEN, function, TERM_NORMAL);
            vfprintf (stream, formatstring, args);
            fprintf (stream, "\n%s%s%s: Aborting program.",
                    TERM_RED TERM_BOLD, function, TERM_NORMAL);
            putchar('\n');
            fflush (stream);
            abort();
    }
    va_end (args);

    fprintf(stream, "\n");
    fflush (stream);
}

void
SysDebug::sysLogMsg (
        const char   *function,
        const char   *file,
        int           line,
        const char   *formatstring,
        ...)
{
    struct      timeval tv;
    FILE       *stream = stdout;
    va_list    args;
    va_start  (args, formatstring);

    gettimeofday (&tv, NULL);
#ifdef MCB_PROFILE_IOS
    fprintf (stream, "%ld.%06d %s%s() file '%s' line %d%s:\n",
            tv.tv_sec - start, tv.tv_usec,
            XTERM_COLOR_16, function,
            file, line, TERM_NORMAL);
#else
    fprintf (stream, "%ld.%06ld %s%s() file '%s' line %d%s:\n",
            tv.tv_sec - start, tv.tv_usec,
            XTERM_COLOR_16, function,
            file, line, TERM_NORMAL);
#endif //IOS
    vfprintf (stream, formatstring, args);

    va_end (args);

    fprintf(stream, "\n\n");
    fflush (stream);
}
#endif

