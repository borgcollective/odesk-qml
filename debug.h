/******************************************************************************
 * The first part of the header can be loaded and loaded again, so we can turn
 * on and off the debug facility for each source file.
 */
//#define DEBUG
//#define WARNING
#define MESSAGE

#ifdef DEBUG_ALL
#  ifndef DEBUG
#    define DEBUG
#  endif
#  ifndef WARNING
#    define DEBUG
#  endif
#  ifndef MESSAGE
#    define DEBUG
#  endif
#endif

/*
 * In production releases, we have to disable all the debug messages
 */
#ifdef PUBLIC_RELEASE
#   undef DEBUG
#   undef WARNING
#   undef MESSAGE
#endif

/*
 * If the debug facility is enabled we also enable all the warning messages.
 */
#if defined(DEBUG) && !defined(WARNING)
#  define WARNING
#endif

#if defined(WARNING) && !defined(CRITICAL)
#  define CRITICAL
#endif

#if defined(MESSAGE)
#  define SYS_MESSAGE(...) SysDebug::sysLogMsg (\
        __func__, \
        __FILE__, \
        __LINE__, \
        __VA_ARGS__)
#else
#  define SYS_MESSAGE(...) { /* Nothing... */ }
#endif

/*
 * The SYS_DEBUG macro is used to print normal debug messages.
 */
#undef SYS_DEBUG
#ifdef DEBUG
#  define SYS_DEBUG(...) SysDebug::sysPrintMsg (\
        QtDebugMsg, \
        __func__, \
        __VA_ARGS__)
#else
#  define SYS_DEBUG(...) { /* Nothing... */ }
#endif

#undef SYS_SYSTEM
#ifdef WARNING
#  define SYS_SYSTEM(...) SysDebug::sysPrintMsg (\
        QtSystemMsg, \
        __func__, \
        __VA_ARGS__)
#else
#  define SYS_SYSTEM(...) { /* Nothing... */ }
#endif

/*
 * The SYS_WARNING is used to print warning messages.
 */
#undef SYS_WARNING
#ifdef WARNING
#  define SYS_WARNING(...) SysDebug::sysPrintMsg (\
        QtWarningMsg, \
        __func__, \
        __VA_ARGS__)
#else
#  define SYS_WARNING(...) { /* Nothing... */ }
#endif

/*
 * The SYS_CRITICAL is used to print warning messages.
 */
#undef SYS_CRITICAL
#ifdef CRITICAL
#  define SYS_CRITICAL(...) SysDebug::sysPrintMsg (\
        QtCriticalMsg, \
        __func__, \
        __VA_ARGS__)
#else
#  define SYS_CRITICAL(...) { /* Nothing... */ }
#endif

/******************************************************************************
 * Here we have those parts that can be loaded only once, so we protect them
 * with DEBUG_H. 
 */
#ifndef DEBUG_H
#define DEBUG_H

#include <QTime>
#include <QtDebug>

#ifdef Q_OS_SYMBIAN
#  define DEBUG_SUPPRESS_COLOR
#endif

#define DEBUG_SUPPRESS_COLOR
#ifdef DEBUG_SUPPRESS_COLOR
#  define TERM_RED     ""
#  define TERM_YELLOW  ""
#  define TERM_GREEN   ""
#  define TERM_BLUE    ""
#  define TERM_NORMAL  ""
#  define TERM_BOLD    ""
#  define TERM_UNDERLINE    ""
#  define XTERM_COLOR_1 ""
#  define XTERM_COLOR_2 ""
#  define XTERM_COLOR_3 ""
#  define XTERM_COLOR_4 ""
#  define XTERM_COLOR_5 ""
#  define XTERM_COLOR_6 ""
#  define XTERM_COLOR_7 ""
#  define XTERM_COLOR_8 ""
#  define XTERM_COLOR_9 ""
#  define XTERM_COLOR_10 ""
#  define XTERM_COLOR_11 ""
#  define XTERM_COLOR_12 ""
#  define XTERM_COLOR_13 ""
#  define XTERM_COLOR_14 ""
#  define XTERM_COLOR_15 ""
#  define XTERM_COLOR_16 ""
#else 
#  define TERM_YELLOW  "\033[1;31m" 
#  define TERM_RED     "\033[1;33m" 
#  define TERM_GREEN   "\033[1;32m"
#  define TERM_BLUE    "\033[1;34m"
#  define TERM_NORMAL  "\033[0;39m"
#  define TERM_BOLD    "\033[1m"
#  define TERM_UNDERLINE    "\033[4m"
#  define XTERM_COLOR_1 "\033[38;5;1m"
#  define XTERM_COLOR_2 "\033[38;5;2m"
#  define XTERM_COLOR_3 "\033[38;5;3m"
#  define XTERM_COLOR_4 "\033[38;5;4m"
#  define XTERM_COLOR_5 "\033[38;5;5m"
#  define XTERM_COLOR_6 "\033[38;5;6m"
#  define XTERM_COLOR_7 "\033[38;5;8m"
#  define XTERM_COLOR_8 "\033[38;5;9m"
#  define XTERM_COLOR_9 "\033[38;5;10m"
#  define XTERM_COLOR_10 "\033[38;5;12m"
#  define XTERM_COLOR_11 "\033[38;5;13m"
#  define XTERM_COLOR_12 "\033[38;5;14m"
#  define XTERM_COLOR_13 "\033[38;5;17m"
// clear dark blue.
#  define XTERM_COLOR_14 "\033[38;5;21m"
// blackish blue
#  define XTERM_COLOR_15 "\033[38;5;19m"
// neon
#  define XTERM_COLOR_16 "\033[38;5;93m"
#endif
void 
segfault_event_handler (
        int signum);

namespace SysDebug
{
    void sysPrintMsg (
        QtMsgType     type,
        const char   *function,
        const char   *formatstring,
        ...);

    void sysLogMsg (
        const char   *function,
        const char   *file,
        int           line,
        const char   *formatstring,
        ...);
};

#define SYS_STR(qstring) (TERM_BOLD+qstring+TERM_NORMAL).toLatin1().constData()

#define SYS_BOOL(boolean) (boolean ? \
        TERM_BOLD "true" TERM_NORMAL : \
        TERM_BOLD "false" TERM_NORMAL)

#define SYS_STR_URL(qstring) \
    (TERM_UNDERLINE TERM_BLUE + qstring + TERM_NORMAL).toUtf8().constData()

#define SYS_STR_FILE(qstring) \
    (TERM_UNDERLINE TERM_GREEN + qstring + TERM_NORMAL).toUtf8().constData()

#define SYS_STR_STRINGLIST(list) \
    (XTERM_COLOR_12 + (list).join("; ") + TERM_NORMAL).toUtf8().constData()

#define SYS_STR_CONFERENCE(conference) \
    (XTERM_COLOR_14 + (conference).name() + TERM_NORMAL).toUtf8().constData()

#define SYS_TIME_STR SYS_STR(QTime::currentTime ().toString ("hh:mm:ss.zzz"))
#endif

