#include <QApplication>
#include "qmlapplicationviewer.h"
#include <QtDeclarative>

#include <QIcon>

#include "odbusinesslogic.h"
#include "odjobslistmodel.h"
#include "odjobsproxymodel.h"
#include "odsqldatabase.h"

#define DEBUG
#include "debug.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    int retval;

    QScopedPointer<QApplication> app(createApplication(argc, argv));
    QmlApplicationViewer  viewer;
    OdBusinessLogic      *businessLogic;

    qmlRegisterType<OdBusinessLogic> (
                "oDeskBackend", 1, 0, "Backend");

    qmlRegisterType<OdJobsListModel> (
                "oDeskBackend", 1, 0, "JobsListModel");

    qmlRegisterType<OdJobsProxyModel> (
                "oDeskBackend", 1, 0, "JobsProxyModel");

    qmlRegisterType<OdSqlDatabase> (
                "oDeskBackend", 1, 0, "SqlDatasase");

    businessLogic = OdBusinessLogic::instance();
    viewer.rootContext()->setContextProperty("backend", businessLogic);
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qrc:///qml/MainWindow.qml"));
    viewer.showExpanded();

    viewer.setWindowIcon(QIcon(":/qml/pixmaps/odeskqml64.png"));
    viewer.setWindowTitle("oDesk QML client");

    retval = app->exec();
    SYS_DEBUG ("Normal logout.");
    businessLogic->logout();
    delete businessLogic;

    return retval;
}
