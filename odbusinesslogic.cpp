#include "odbusinesslogic.h"

#include <QTimer>
#include "odsettings.h"

#include <unistd.h> // for sleep

static const QString serverNameKey       ("sqlservername");
static const QString databaseNameKey     ("sqldatabasename");
static const QString databaseUserNameKey ("sqlusername");
static const QString databasePasswordKey ("sqlpassword");
static const QString databaseEnabledKey  ("sqlenabled");

#define DEBUG
#include "debug.h"

OdBusinessLogic *OdBusinessLogic::sm_instance;

OdBusinessLogic::OdBusinessLogic(QObject *parent) :
    QObject(parent),
    m_sqlStatus (OdSqlDatabase::SqlStatusUnknown)
{
    OdSettings *settings = OdSettings::instance();

    sm_instance = this;
    m_databaseServerName = settings->getString(serverNameKey);
    m_databaseName       = settings->getString(databaseNameKey);
    m_databaseUserName   = settings->getString(databaseUserNameKey);
    m_databasePassword   = settings->getString(databasePasswordKey);
    m_databasEnabled     = settings->getBoolean(databaseEnabledKey);

    if (m_databasEnabled &&
            !m_databaseServerName.isEmpty() &&
            !m_databaseName.isEmpty() &&
            !m_databaseUserName.isEmpty() &&
            !m_databasePassword.isEmpty())
        loginSqlServer(m_databaseUserName, m_databasePassword);

}

OdBusinessLogic*
OdBusinessLogic::instance()
{
    if (!sm_instance)
        sm_instance = new OdBusinessLogic;

    return sm_instance;
}

/******************************************************************************
 * Methods to handle the SQL database. These could go into a separate class.
 */
QString
OdBusinessLogic::sqlServerName () const
{
    return m_databaseServerName;
}

QString
OdBusinessLogic::sqlDatabaseName () const
{
    return m_databaseName;
}

QString
OdBusinessLogic::sqlUserName () const
{
    SYS_DEBUG ("*** retval: '%s'", SYS_STR(m_databaseUserName));
    return m_databaseUserName;
}

QString
OdBusinessLogic::sqlPassword () const
{
    return m_databasePassword;
}

void
OdBusinessLogic::sqlStatusChangedSlot (
        OdSqlDatabase::OdSqlStatus status)
{
    SYS_DEBUG ("");
    m_sqlStatus = status;
    emit sqlStatusChanged();
}

OdSqlDatabase::OdSqlStatus
OdBusinessLogic::sqlStatus() const
{
    return m_sqlStatus;
}

/**
 * This should be sqlLogout
 */
void
OdBusinessLogic::logout ()
{
    SYS_DEBUG ("Logout from SQL database");
    QMetaObject::invokeMethod (
            m_sqlDatabase, "storeStatus", Qt::QueuedConnection,
            Q_ARG(bool, false));
    
    QMetaObject::invokeMethod (
            m_sqlDatabase, "disconnectFromDatabase",
            Qt::QueuedConnection);
}


bool
OdBusinessLogic::databaseEnabled () const
{
    return m_databasEnabled;
}

void
OdBusinessLogic::setDatabaseEnabled (
        bool enabled)
{
    if (enabled == m_databasEnabled)
        return;

    OdSettings::instance()->setBoolean(databaseEnabledKey, enabled);
    m_databasEnabled = enabled;
    emit databaseEnabledChanged ();
}

void
OdBusinessLogic::connectSqlServer (
        const QString &serverName,
        const QString &databaseName)
{
    OdSettings *settings = OdSettings::instance();

    SYS_DEBUG ("*** serverName  : %s", SYS_STR(serverName));
    SYS_DEBUG ("*** databaseName: %s", SYS_STR(databaseName));
    m_databaseServerName = serverName;
    m_databaseName       = databaseName;
    emit sqlServerNameChanged();
    emit sqlDatabaseNameChanged();

    settings->setString(serverNameKey, m_databaseServerName);
    settings->setString(databaseNameKey, m_databaseName);

    m_stateName = "sqllogindialog";
    stateNameChanged();
}

void
OdBusinessLogic::loginSqlServer (
        const QString &userName,
        const QString &password)
{
    OdSettings *settings = OdSettings::instance();
    bool        success;

    SYS_DEBUG ("*** userName    : %s", SYS_STR(userName));
    m_databaseUserName = userName;
    m_databasePassword = password;
    settings->setString(databaseUserNameKey, userName);
    settings->setString(databasePasswordKey, password);
    emit sqlUserNameChanged();
    emit sqlPasswordChanged();


    m_stateName = "";
    stateNameChanged();

    m_databaseThread = new QThread;
    m_sqlDatabase = new OdSqlDatabase;

    m_sqlDatabase->moveToThread(m_databaseThread);
    m_databaseThread->start ();

    if (m_jobsModel) {
        success = connect (
             m_jobsModel, SIGNAL(disabledJobIdAdded(QString)),
             m_sqlDatabase, SLOT(addIgnoredJob(QString)));
        Q_ASSERT (success);

        success = connect (
            m_sqlDatabase, SIGNAL(ignoredJobsReceived (QSet<QString>)),
            m_jobsModel, SIGNAL(ignoredJobsReceived(QSet<QString>)));
        Q_ASSERT(success);
    }

    success = connect (
             m_sqlDatabase, SIGNAL(statusChanged(OdSqlDatabase::OdSqlStatus)),
             this, SLOT(sqlStatusChangedSlot(OdSqlDatabase::OdSqlStatus)));
    Q_ASSERT(success);

    QMetaObject::invokeMethod (
            m_sqlDatabase, "setDatabaseInfo", Qt::QueuedConnection,
            Q_ARG(QString, m_databaseServerName),
            Q_ARG(QString, m_databaseName),
            Q_ARG(QString, userName),
            Q_ARG(QString, password));

    QTimer::singleShot(500, m_sqlDatabase, SLOT(connectToDatabase()));
    Q_UNUSED(success)
}

void
OdBusinessLogic::loginSqlServerCancelled ()
{
    m_stateName = "";
    stateNameChanged();
}

/******************************************************************************
 *
 */
QString
OdBusinessLogic::stateName () const
{
    return m_stateName;
}

OdJobsListModel *
OdBusinessLogic::jobsModel ()
{
    if (!m_jobsModel) {
        m_jobsModel = new OdJobsListModel;
        m_jobsProxyModel = new OdJobsProxyModel;
        m_jobsProxyModel->setModel(m_jobsModel);

        if (m_sqlDatabase) {
            bool success;

            success = connect (
                 m_jobsModel, SIGNAL(disabledJobIdAdded(QString)),
                 m_sqlDatabase, SLOT(addIgnoredJob(QString)));
            Q_ASSERT (success);

            success = connect (
                m_sqlDatabase, SIGNAL(ignoredJobsReceived (QSet<QString>)),
                m_jobsModel, SIGNAL(ignoredJobsReceived(QSet<QString>)));
            Q_ASSERT(success);
        }
    }

    SYS_DEBUG ("->");
    return m_jobsModel.data();
}

OdJobsProxyModel *
OdBusinessLogic::jobsProxyModel ()
{
    if (!m_jobsModel) {
        m_jobsModel = new OdJobsListModel;
        m_jobsProxyModel = new OdJobsProxyModel;
        m_jobsProxyModel->setModel(m_jobsModel);

        if (m_sqlDatabase) {
            bool success;

            success = connect (
                 m_jobsModel, SIGNAL(disabledJobIdAdded(QString)),
                 m_sqlDatabase, SLOT(addIgnoredJob(QString)));
            Q_ASSERT (success);

            success = connect (
                m_sqlDatabase, SIGNAL(ignoredJobsReceived (QSet<QString>)),
                m_jobsModel, SIGNAL(ignoredJobsReceived(QSet<QString>)));
            Q_ASSERT(success);
        }
    }

    SYS_DEBUG ("-> %p", m_jobsProxyModel.data());
    return m_jobsProxyModel.data();
}

void
OdBusinessLogic::loadJobDetails (
        const QString &jobId)
{
    SYS_DEBUG ("*** jobId: '%s'", SYS_STR(jobId));
    Q_ASSERT (m_jobsModel);
    m_jobsModel->loadJobDetails(jobId);
}
