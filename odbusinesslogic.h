#ifndef ODBUSINESSLOGIC_H
#define ODBUSINESSLOGIC_H

#include <QObject>
#include <QString>
#include <QPointer>
#include <QThread>

#include "odjobslistmodel.h"
#include "odjobsproxymodel.h"
#include "odsqldatabase.h"

class OdBusinessLogic : public QObject
{
    Q_OBJECT
    Q_PROPERTY (
            QString stateName
            READ stateName
            NOTIFY stateNameChanged)

    Q_PROPERTY (
            QObject *jobsModel
            READ jobsModel
            CONSTANT)

    Q_PROPERTY (
            QObject *jobsProxyModel
            READ jobsProxyModel
            CONSTANT)

    Q_PROPERTY (
            bool databaseEnabled
            READ databaseEnabled
            WRITE setDatabaseEnabled
            NOTIFY databaseEnabledChanged)

    Q_PROPERTY (
            QString sqlServerName
            READ sqlServerName
            NOTIFY sqlServerNameChanged)

    Q_PROPERTY (
            QString sqlDatabaseName
            READ sqlDatabaseName
            NOTIFY sqlDatabaseNameChanged)

    Q_PROPERTY (
            QString sqlUserName
            READ sqlUserName
            NOTIFY sqlUserNameChanged)

    Q_PROPERTY (
            QString sqlPassword
            READ sqlPassword
            NOTIFY sqlPasswordChanged)

    Q_PROPERTY (
            OdSqlDatabase::OdSqlStatus sqlStatus
            READ sqlStatus
            NOTIFY sqlStatusChanged)

    public:
        // This is very disturbing: QML doesn't like singletons, need to find a
        // solution.
        OdBusinessLogic(QObject *parent = 0);
        static OdBusinessLogic *instance();

        bool databaseEnabled () const;
        void setDatabaseEnabled (bool enabled);

        QString sqlServerName () const;
        QString sqlDatabaseName () const;
        QString sqlUserName () const;
        QString sqlPassword () const;
        OdSqlDatabase::OdSqlStatus sqlStatus() const;

        Q_INVOKABLE void logout ();
        QString stateName () const;
        OdJobsListModel *jobsModel ();
        OdJobsProxyModel *jobsProxyModel ();

        Q_INVOKABLE void connectSqlServer (
                const QString &serverName,
                const QString &databaseName);

        Q_INVOKABLE void loginSqlServer (
                const QString &userName,
                const QString &password);

        Q_INVOKABLE void loginSqlServerCancelled ();

        Q_INVOKABLE void loadJobDetails (const QString &jobId);

    signals:
        void stateNameChanged ();
        void databaseEnabledChanged ();
        void sqlServerNameChanged ();
        void sqlDatabaseNameChanged ();
        void sqlUserNameChanged ();
        void sqlPasswordChanged ();
        void sqlStatusChanged();
    
    public slots:
    private slots:
        void sqlStatusChangedSlot (OdSqlDatabase::OdSqlStatus status);

    private:
        //OdBusinessLogic(QObject *parent = 0);

    private:
        static OdBusinessLogic *sm_instance;

        QString                    m_stateName;
        bool                       m_databasEnabled;
        QString                    m_databaseServerName;
        QString                    m_databaseName;
        QString                    m_databaseUserName;
        QString                    m_databasePassword;
        QPointer<OdJobsListModel>  m_jobsModel;
        QPointer<OdJobsProxyModel> m_jobsProxyModel;
        QPointer<OdSqlDatabase>    m_sqlDatabase;
        QPointer<QThread>          m_databaseThread;
        OdSqlDatabase::OdSqlStatus m_sqlStatus;
};

#endif
