# Add more folders to ship with the application, here
#folder_01.source = qml/odeskqml
#folder_01.target = qml
#DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
#QML_IMPORT_PATH =

#QMAKE_LFLAGS += -static-libgcc
symbian:TARGET.UID3 = 0xE759984D

# Smart Installer package's UID
# This UID is from the protected range and therefore the package will
# fail to install if self-signed. By default qmake uses the unprotected
# range value if unprotected UID is defined for the application and
# 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += NetworkServices

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
# CONFIG += qdeclarative-boostable

# Add dependency to Symbian components
# CONFIG += qt-components
CONFIG +=  \
    debug \
    silent

QT += \
    xml \
    network \
    sql

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    debug.cpp \
    odjobslistmodel.cpp \
    odjob.cpp \
    odjobsproxymodel.cpp \
    odsettings.cpp \
    odsqldatabase.cpp \
    odbusinesslogic.cpp

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

RESOURCES += \
    qmlresources.qrc

icon.files += qml/pixmaps/odeskqml64.png
icon.path = /usr/share/pixmaps

desktop.files += odeskqml.desktop
desktop.path = /usr/share/applications

INSTALLS += \
    icon \
    desktop

HEADERS += \
    debug.h \
    odjobslistmodel.h \
    odjob.h \
    odjobsproxymodel.h \
    odsettings.h \
    odsqldatabase.h \
    odbusinesslogic.h

OTHER_FILES += \
    qml/BottomShadow.qml \
    qml/Button.qml \
    qml/Circle.qml \
    qml/CheckButton.qml \
    qml/theme.js \
    qml/TopMenu.qml \
    qml/SkillsModel.qml \
    qml/SettingsItem.qml \
    qml/SearchPage.qml \
    qml/SearchModel.qml \
    qml/SearchBar.qml \
    qml/ScrollBar.qml \
    qml/ProgressIndicator.qml \
    qml/Pager.qml \
    qml/MainWindow.qml \
    qml/LoginPage.qml \
    qml/ListItemDelegate.qml \
    qml/GreenButton.qml \
    qml/Frame.qml \
    qml/FlickableText.qml \
    qml/Entry.qml \
    qml/TopMenuItem.qml \
    qml/SlideSwitch.qml \
    qml/SectionHeader.qml \
    qml/Dialog.qml \
    qml/SlideButton.qml \
    qml/ListLazyLoaderItem.qml \
    qml/SettingsPage.qml

