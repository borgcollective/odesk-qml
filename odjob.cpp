#include "odjob.h"

#define DEBUG
#define WARNING
#include "debug.h"

QStringList OdJob::sm_webAppKeywords;

OdJob::OdJob(QObject *parent) :
    QObject(parent),
    m_amount (0),
    m_hourlyRate (0),
    m_hoursPerWeek (0)
{
    if (sm_webAppKeywords.isEmpty())
        sm_webAppKeywords << "php" << "wordpress";
}

OdJob::OdJob (
        const OdJob &orig) :
    QObject (),
    m_title (orig.m_title),
    m_description (orig.m_description),
    m_startDateString (orig.m_startDateString),
    m_requiredSkillsString (orig.m_requiredSkillsString),
    m_engagementString (orig.m_engagementString),
    m_jobTypeString (orig.m_jobTypeString),
    m_categorySeoString (orig.m_categorySeoString),
    m_recordId (orig.m_recordId),
    m_amount (orig.m_amount),
    m_hourlyRate (orig.m_hourlyRate),
    m_hoursPerWeek (orig.m_hoursPerWeek),
    m_detailsXml (orig.m_detailsXml)
{
    cache ();
}

OdJob &
OdJob::operator= (
        const OdJob &rhs)
{
    if (this == &rhs)
        return *this;

    m_title = rhs.m_title;
    m_description = rhs.m_description;
    m_startDateString = rhs.m_startDateString;
    m_requiredSkillsString = rhs.m_requiredSkillsString;
    m_engagementString = rhs.m_engagementString;
    m_jobTypeString = rhs.m_jobTypeString;
    m_categorySeoString = rhs.m_categorySeoString;
    m_recordId = rhs.m_recordId;
    m_amount = rhs.m_amount;
    m_hourlyRate = rhs.m_hourlyRate;
    m_hoursPerWeek = rhs.m_hoursPerWeek;
    m_detailsXml = rhs.m_detailsXml;

    cache();
    return *this;
}

OdJob::OdJob (
        QDomNode node)
{
    node = node.firstChild();

    SYS_DEBUG ("------------------------");
    while(!node.isNull()) {
        QDomElement element = node.toElement();
        if (!element.isNull()) {
            //SYS_DEBUG ("*** name: %s", SYS_STR(element.nodeName()));
            if (element.nodeName() == "op_title") {
                SYS_DEBUG ("*** title: %s", SYS_STR(element.text()));
                m_title = element.text();
            } else if (element.nodeName() == "op_description")
                m_description = element.text();
            else if (element.nodeName() == "op_start_date")
                m_startDateString = element.text();
            else if (element.nodeName() == "op_required_skills") {
                m_requiredSkillsString = element.text();
                m_requiredSkillsString.replace (",", ", ");
            } else if (element.nodeName() == "op_engagement")
                m_engagementString = element.text();
            else if (element.nodeName() == "job_type")
                m_jobTypeString = element.text();
            else if (element.nodeName() == "op_recno") {
                SYS_DEBUG ("*** recno : '%s'", SYS_STR(element.text()));
                m_recordId = element.text();
            } else if (element.nodeName() == "amount") {
                m_amount = element.text().toInt();
            } else if (element.nodeName() == "op_pref_hourly_rate_max") {
                m_hourlyRate = element.text().toInt();
            } else if (element.nodeName() == "hours_per_week") {
                //SYS_DEBUG ("*** h/w  : %s", SYS_STR(element.text()));
                m_hoursPerWeek = element.text().toInt();
            } else if (element.nodeName() == "op_job_category_seo") {
                m_categorySeoString = element.text();
            } else if (element.nodeName() == "op_est_duration") {
                SYS_DEBUG ("*** op_est_duration: '%s'", SYS_STR(element.text()));
            } else if (element.nodeName() == "op_time_posted") {
                SYS_DEBUG ("*** op_time_posted : '%s'", SYS_STR(element.text()));
            } else if (element.nodeName() == "op_avg_hourly_rate_active_interviewees") {
                SYS_DEBUG ("*** op_avg_hourly_rate_active_interviewees : '%s'", SYS_STR(element.text()));
            } else if (element.nodeName() == "op_avg_hourly_rate_active_interviewees") {
                SYS_DEBUG ("*** offers_total   : '%s'", SYS_STR(element.text()));
            }
        }

        node = node.nextSibling();
    }

    cache ();
}

void
OdJob::setDetailsXml (
        const QString &details)
{
    m_detailsXml = details;
    emit detailsXmlChanged();
}

QString
OdJob::detailsXml () const
{
    return m_detailsXml;
}

QString
OdJob::title () const
{
    return m_title;
}

QString
OdJob::description () const
{
    return m_description;
}

QString
OdJob::categorySeoString () const
{
    return m_categorySeoString;
}

QString
OdJob::startDateString () const
{
    return m_startDateString;
}

QString
OdJob::requiredSkillsString () const
{
    return m_requiredSkillsString;
}

QString
OdJob::engagementString () const
{
    return m_engagementString;
}

QString
OdJob::jobTypeString () const
{
    return m_jobTypeString;
}

QString
OdJob::recordId () const
{
    return m_recordId;
}

int
OdJob::amount () const
{
    return m_amount;
}

/**
 * \returns The preferred hourly rate max.
 */
int
OdJob::hourlyRate () const
{
    return m_hourlyRate;
}

int
OdJob::hoursPerWeek () const
{
    return m_hoursPerWeek;
}

bool
OdJob::isCheap () const
{
    if (m_amount > 0 && m_amount < 25)
        return true;

    if (m_hourlyRate > 0 && m_hourlyRate < 15)
        return true;

    return false;
}

bool
OdJob::isHighPay () const
{
    if (m_amount >= 1000)
        return true;

    if (m_hourlyRate >= 30)
        return true;

    return false;
}

bool
OdJob::isWebJob () const
{
    QString category = m_categorySeoString.toLower();
    if (category.startsWith("web "))
        return true;

    foreach (const QString &word, sm_webAppKeywords)
        if (m_titleLower.contains(word) ||
                m_descriptionLower.contains(word) ||
                m_requiredSkillsLower.contains(word))
            return true;

    return false;
}

bool
OdJob::isProgrammingJob () const
{
    QString category = m_categorySeoString.toLower();
    if (category.contains("programming") ||
            category.contains("software development"))
        return true;

    return false;
}

bool
OdJob::isMobileJob() const
{
    if (m_categorySeoString.toLower().contains("mobile development"))
        return true;
    return false;
}

bool
OdJob::isHourly() const
{
    return m_jobTypeString == "Hourly";
}

bool
OdJob::hasDetailsXml() const
{
    return !m_detailsXml.isEmpty();
}

/**
 * Some values are cached for the sake of speed. This method will refresh
 * those values. This is a simple way to handle cached data, but this
 * class is not very dinamyc; it doesn't change once it is created.
 */
void
OdJob::cache () const
{
    m_titleLower = m_title.toLower ();
    m_descriptionLower = m_description.toLower();
    m_requiredSkillsLower = m_requiredSkillsString.toLower();
    m_requiredSkillsLower.replace(",", " ");
}
