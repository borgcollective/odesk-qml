#ifndef ODJOB_H
#define ODJOB_H

#include <QObject>
#include <QString>
#include <QDomNode>
#include <QStringList>

class OdJob : public QObject
{
    Q_OBJECT
    Q_PROPERTY (
            QString detailsXml
            READ detailsXml
            WRITE setDetailsXml
            NOTIFY detailsXmlChanged)

    public:
        explicit OdJob(QObject *parent = 0);
        OdJob (const OdJob &orig);
        OdJob (QDomNode node);

        OdJob &operator= (const OdJob &rhs);

        QString title () const;
        QString description () const;
        QString categorySeoString () const;
        QString startDateString () const;
        QString requiredSkillsString () const;
        QString engagementString () const;
        QString jobTypeString () const;
        QString recordId () const;

        void setDetailsXml (const QString &details);
        QString detailsXml () const;

        int amount () const;
        int hourlyRate () const;
        int hoursPerWeek () const;

        bool isCheap () const;
        bool isHighPay () const;
        bool isWebJob () const;
        bool isMobileJob () const;
        bool isProgrammingJob () const;
        bool isHourly () const;
        bool hasDetailsXml () const;

    signals:
        void detailsXmlChanged ();

    public slots:

    protected:
        void cache () const;

    private:
        static QStringList sm_webAppKeywords;

        QString   m_title;
        QString   m_description;
        QString   m_startDateString;
        QString   m_requiredSkillsString;
        QString   m_engagementString;
        QString   m_jobTypeString;
        QString   m_categorySeoString;
        QString   m_recordId;
        int       m_amount;
        int       m_hourlyRate;
        int       m_hoursPerWeek;
        QString   m_detailsXml;

        // Cached values.
        mutable QString   m_titleLower;
        mutable QString   m_descriptionLower;
        mutable QString   m_requiredSkillsLower;
};

#endif
