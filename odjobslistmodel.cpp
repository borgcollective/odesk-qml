#include "odjobslistmodel.h"

#include <QUrl>
#include <QNetworkRequest>
#include <QDomDocument>
#include <QDomElement>
#include <QHash>
#include <QByteArray>
#include <QTimer>

#define DEBUG
#include "debug.h"

/**
 * (1) Normal hourly paid job
 * (2) Normal fixed price non-hourly job
 * (3) High money, well paying job with hourly rate
 * (4) High money, well paying fixed price job
 * (5) Normal jobs related to desktop computer software development
 * (6) Normal jobs related to web page development
 * (7) Normal jobs related to mobile application development
 * (8) Same as (5) but well paying
 * (9) Same as (6) but well paying
 * (10) Same as (7) but well paying
 */
OdJobsListModel::OdJobsListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_loading(false),
    m_fromIndex(0)
{
    qRegisterMetaType<QSet<QString> > ();
    m_rolenames[JobTitleRole]       = "jobTitle";
    m_rolenames[JobDescriptionRole] = "op_description";
    m_rolenames[JobDetailsXmlRole]  = "jobDetailsXml";
    m_rolenames[JobCategorySeoRole] = "jobCategory";
    m_rolenames[JobStartDateRole]   = "op_start_date";
    m_rolenames[JobEngagementRole]  = "op_engagement";
    m_rolenames[JobReqSkillsRole]   = "op_req_skills";
    m_rolenames[JobTypeRole]        = "job_type";
    m_rolenames[JobRecno]           = "jobId";
    m_rolenames[JobAmount]          = "m_amount";
    m_rolenames[JobHourlyRate]      = "jobHourlyRate";
    m_rolenames[JobIsCheap]         = "jobIsCheap";
    m_rolenames[JobIsWebRelated]    = "jobIsWebRelated";
    m_rolenames[JobIconName]        = "jobIconName";

    // Qt 5.0 does not have this setRoleNames() method, has to use roleNames()
    // virtual instead. Previous Qt versions does not work that way.
    #if QT_VERSION < 0x050000
    setRoleNames (m_rolenames);
    #endif
}

OdJobsListModel::~OdJobsListModel()
{
    SYS_DEBUG ("");
}

QHash<int, QByteArray>
OdJobsListModel::roleNames () const
{
    return m_rolenames;
}

bool
OdJobsListModel::loading () const
{
    return m_loading;
}

QString
OdJobsListModel::search () const
{
    return m_searchString;
}

void
OdJobsListModel::appendDisabledJobId (
        const QString &id)
{
    emit disabledJobIdAdded (id);
}

void
OdJobsListModel::setSearch (
        const QString &search)
{
    beginResetModel();
    m_jobs.clear();
    endResetModel();

    m_loading       = true;
    m_searchString  = search;
    m_fromIndex     = 0;

    emit loadingChanged();
    emit searchChanged();

    m_networkReply = m_netManager.get(QNetworkRequest(QUrl(searchUrl())));
    connect (m_networkReply.data(), SIGNAL(readyRead()),
             this, SLOT(readyRead()));
}


void
OdJobsListModel::readyRead ()
{
    int     nLoaded (0);
    QString stuff = QString::fromUtf8(m_networkReply->readAll ());

    QDomDocument document;

    document.setContent (stuff, true);

    QDomElement docElem = document.documentElement();
    QDomNode node = docElem.firstChild();

    while(!node.isNull()) {
        QDomElement element = node.toElement();
        if (!element.isNull()) {
            if (element.nodeName() == "jobs")
                nLoaded += loadJobs (node);
        }

        node = node.nextSibling();
    }

    m_loading = false;
    emit loadingChanged();

    if (nLoaded > 0 && m_fromIndex < 60) {
        m_fromIndex += nLoaded;
        QTimer::singleShot(5000, this, SLOT(readNext()));
    }
}

void
OdJobsListModel::readyReadJobDetails()
{
    QString stuff = QString::fromUtf8(m_jobDetailsReply->readAll ());
    QString jobId = m_jobDetailsReply->objectName();
    SYS_DEBUG ("*** objectName: %s", SYS_STR(jobId));
    SYS_DEBUG ("*** stuff     : %s", SYS_STR(stuff));

    int idx = findJobIdx(jobId);
    if (idx < 0)
        return;

    QModelIndex i = index (idx, 0);
    m_jobs[idx].setDetailsXml(stuff);
    emit dataChanged (i, i);
}

void
OdJobsListModel::readNext()
{
    SYS_DEBUG ("m_fromIndex: %d", m_fromIndex);
    if (m_fromIndex > 60)
        return;

    m_networkReply = m_netManager.get(QNetworkRequest(QUrl(searchUrl())));
    connect (m_networkReply.data(), SIGNAL(readyRead()),
             this, SLOT(readyRead()));
}

int
OdJobsListModel::loadJobs (
        QDomNode node)
{
    int retval (0);

    node = node.firstChild();

    while(!node.isNull()) {
        QDomElement element = node.toElement();
        if (!element.isNull() && element.nodeName() == "job") {
            int   nTh = m_jobs.size();

            OdJob job(node);
            if (!job.title().isEmpty()) {
                beginInsertRows(QModelIndex(), nTh, nTh);
                m_jobs.append (job);
                endInsertRows();
                ++retval;
            }
        }

        node = node.nextSibling();
    }

    return retval;
}

QString
OdJobsListModel::searchUrl () const
{
    QString url;
    QString s;

    s = QString::fromUtf8(QUrl::toPercentEncoding(m_searchString));
    url = QString (
        "https://www.odesk.com/api/profiles/v1/search/jobs.xml"
        "?sort=%1&page=%2;20&qs=%3&t=%4&q=%5").
            arg("date_posted;D").
            arg(m_fromIndex).
            arg("").
            arg("").
            arg(s);

    SYS_WARNING ("%s", SYS_STR(url));
    return url;
}

QString
OdJobsListModel::jobDetailsUrl (
        const QString &jobId) const
{
    return "https://www.odesk.com/api/profiles/v1/jobs/" +
            jobId + ".xml";
}

/******************************************************************************
 * QAbstractListModel virtual methods to access model data.
 */
int
OdJobsListModel::rowCount (
        const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_jobs.size();
}

QVariant
OdJobsListModel::data (
        const QModelIndex &index,
        int                role) const
{
    QVariant retval;

    if (index.row() < 0 || index.row() >= m_jobs.size()) {
        SYS_WARNING ("Index is %d (0 -- %d)", index.row(), m_jobs.size() - 1);
        return retval;
    }

    const OdJob &job = m_jobs[index.row()];

    switch (role) {
        case JobTitleRole:
            retval = job.title();
            break;

        case JobDescriptionRole:
            retval = job.description();
            break;

        case JobDetailsXmlRole:
            retval = job.detailsXml();
            break;

        case JobCategorySeoRole:
            retval = job.categorySeoString();
            break;

        case JobStartDateRole:
            retval = job.startDateString();
            break;

        case JobEngagementRole:
            retval = job.engagementString();
            break;

        case JobReqSkillsRole:
            retval = job.requiredSkillsString();
            break;

        case JobTypeRole:
            retval = job.jobTypeString();
            break;

        case JobRecno:
            retval = job.recordId();
            break;

        case JobAmount:
            retval = job.amount();
            break;

        case JobHourlyRate:
            retval = job.hourlyRate();
            break;

        case JobIsCheap:
            retval = job.isCheap();
            break;

        case JobIsWebRelated:
            retval = job.isWebJob();
            break;

        case JobIconName:
        if (job.isMobileJob()) {
            if (job.isHighPay())
                retval = "icon-mobile-high";
            else
                retval = "icon-mobile-normal";
        } else if (job.isWebJob()) {
            if (job.isHighPay())
                retval = "icon-web-high";
            else
                retval = "icon-web-normal";
        } else if (job.isProgrammingJob()) {
            if (job.isHighPay())
                retval = "icon-software-high";
            else
                retval = "icon-software-normal";
        } else if (job.isHighPay()) {
            if (job.isHourly())
                retval = "icon-high-hourly";
            else
                retval = "icon-high-fixed";
            break;

        } else {
            if (job.isHourly())
                retval = "icon-normal-hourly";
            else
                retval = "icon-normal-fixed";
            break;
        }
    }

    return retval;
}

void
OdJobsListModel::loadJobDetails (
        const QString &jobId)
{
    int idx;

    SYS_DEBUG ("*** jobId: '%s'", SYS_STR(jobId));
    return;

    // We can't do nothing if the job is not stored here...
    // Should we do this like this?
    idx = findJobIdx(jobId);
    if (idx < 0)
        return;

    SYS_DEBUG ("found...");
    // If the details already downloaded we don't load it. Normal caching...
    if (m_jobs[idx].hasDetailsXml())
        return;

    QString url = jobDetailsUrl(jobId);
    m_jobDetailsReply = m_netManager.get(QNetworkRequest(QUrl(url)));
    m_jobDetailsReply->setObjectName(jobId);
    connect (m_jobDetailsReply.data(), SIGNAL(readyRead()),
             this, SLOT(readyReadJobDetails()));
}

/******************************************************************************
 * Low level private methods.
 */
int
OdJobsListModel::findJobIdx (
        const QString &jobId)
{
    int retval (-1);
    for (int i = 0; i < m_jobs.size(); ++i) {
        if (m_jobs[i].recordId() == jobId) {
            retval = i;
            break;
        }
    }

    return retval;
}
