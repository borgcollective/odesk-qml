#ifndef ODJOBSLISTMODEL_H
#define ODJOBSLISTMODEL_H

#include <QAbstractListModel>
#include <QString>
#include <QNetworkAccessManager>
#include <QPointer>
#include <QNetworkReply>
#include <QVector>
#include <QPointer>
#include <QSet>

#include "odjob.h"

class QDomNode;

class OdJobsListModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(
            bool loading
            READ loading
            NOTIFY loadingChanged)

    Q_PROPERTY(
            QString search
            READ search
            WRITE setSearch
            NOTIFY searchChanged)

    public:
        enum JobsRoles {
            JobTitleRole,
            JobDescriptionRole,
            JobDetailsXmlRole,
            JobCategorySeoRole,
            JobStartDateRole,
            JobEngagementRole,
            JobReqSkillsRole,
            JobTypeRole,
            JobRecno,
            JobAmount,
            JobHourlyRate,
            JobIsCheap,
            JobIsWebRelated,
            JobIconName
        };

        OdJobsListModel (QObject *parent = 0);
        ~OdJobsListModel ();

        int rowCount (
                const QModelIndex &parent = QModelIndex()) const;

        QVariant data (
                const QModelIndex &index,
                int                role = Qt::DisplayRole) const;

        void loadJobDetails (const QString &jobId);

        /*
         *
         */
        bool loading () const;

        QString search () const;
        void setSearch (const QString &search);
        virtual QHash<int, QByteArray> roleNames () const;

        void appendDisabledJobId (const QString &id);

    signals:
        void loadingChanged ();
        void searchChanged ();
        void disabledJobIdAdded (const QString id);
        void ignoredJobsReceived (const QSet<QString> ids);

    public slots:
    
    private slots:
        void readyRead ();
        void readyReadJobDetails ();
        void readNext ();

    private:
        int loadJobs (QDomNode node);
        QString searchUrl () const;
        QString jobDetailsUrl (const QString &jobId) const;
        int findJobIdx (const QString &jobId);

    private:
        bool                     m_loading;
        QString                  m_searchString;
        int                      m_fromIndex;
        QNetworkAccessManager    m_netManager;
        QPointer<QNetworkReply>  m_networkReply;
        QPointer<QNetworkReply>  m_jobDetailsReply;
        QVector<OdJob>           m_jobs;
        QHash<int, QByteArray>   m_rolenames;
};

Q_DECLARE_METATYPE(QSet<QString>);
#endif
