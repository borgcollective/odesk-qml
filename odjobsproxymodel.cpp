#include "odjobsproxymodel.h"

#include <QStringList>
#include "odsettings.h"

#define WARNING
#define DEBUG
#include "debug.h"

OdJobsProxyModel::OdJobsProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent),
    m_filtersEnabled (true)
{
    m_filtersEnabled = LOADBOOL("filtersenabled", true);
    for (int n = FilterNotForPeanuts; n < LastFilter; ++n) {
        QString name = QString("filter%1").arg(FilterType(n));
        m_filtersOnOff[FilterType(n)] = LOADBOOL(name, false);
    }
}

void
OdJobsProxyModel::setFiltersEnabled (
        bool value)
{
    if (m_filtersEnabled == value)
        return;

    m_filtersEnabled = value;
    emit filtersEnabledChanged ();
    invalidateFilter();

    SAVEBOOL("filtersenabled", value);
}

bool
OdJobsProxyModel::filtersEnabled() const
{
    return m_filtersEnabled;
}

void
OdJobsProxyModel::setNotForPeanuts (
        bool value)
{
    if (filterIsOn(FilterNotForPeanuts) == value)
        return;

    setFilterIsOn(FilterNotForPeanuts, value);
    emit notForPeanutsChanged ();
    invalidateFilter();
    SAVEBOOL(QString("filter%1").arg(FilterNotForPeanuts), value);
}

bool
OdJobsProxyModel::notForPeanuts () const
{
    return filterIsOn(FilterNotForPeanuts);
}

void
OdJobsProxyModel::setNotPhpCowboy (
        bool value)
{
    if (filterIsOn(FilterNotPhpCowboy) == value)
        return;

    setFilterIsOn(FilterNotPhpCowboy, value);
    emit notPhpCowboyChanged();
    invalidateFilter();
    SAVEBOOL(QString("filter%1").arg(FilterNotPhpCowboy), value);
}

bool
OdJobsProxyModel::notPhpCowboy () const
{
    return filterIsOn(FilterNotPhpCowboy);
}

void
OdJobsProxyModel::setNoiPhone (
        bool value)
{
    if (filterIsOn(FilterNoiPhone) == value)
        return;

    setFilterIsOn(FilterNoiPhone, value);
    emit noiPhoneChanged();
    invalidateFilter();
    SAVEBOOL(QString("filter%1").arg(FilterNoiPhone), value);
}

bool
OdJobsProxyModel::noiPhone () const
{
    return filterIsOn(FilterNoiPhone);
}

void
OdJobsProxyModel::setNoAndroid (
        bool value)
{
    if (filterIsOn(FilterNoAndroid) == value)
        return;

    setFilterIsOn(FilterNoAndroid, value);
    emit noAndroidChanged();
    invalidateFilter();
    SAVEBOOL(QString("filter%1").arg(FilterNoAndroid), value);
}

bool
OdJobsProxyModel::noAndroid () const
{
    return filterIsOn(FilterNoAndroid);
}

void
OdJobsProxyModel::setNoGreyComputing (
        bool value)
{
    if (filterIsOn(FilterNoGreyComputing) == value)
        return;

    setFilterIsOn(FilterNoGreyComputing, value);
    emit noGreyComputingChanged();
    invalidateFilter();
    SAVEBOOL(QString("filter%1").arg(FilterNoGreyComputing), value);
}

bool
OdJobsProxyModel::noGreyComputing () const
{
    return filterIsOn(FilterNoGreyComputing);
}

void
OdJobsProxyModel::setModel (
        OdJobsListModel *model)
{
    m_sourceModel = model;
    setSourceModel (m_sourceModel);
    setDynamicSortFilter (true);
    setSortRole (OdJobsListModel::JobCategorySeoRole);
    setSortLocaleAware (true);
    sort (0);

    if (model) {
        bool success;
        success = connect (
            m_sourceModel, SIGNAL(ignoredJobsReceived (const QSet<QString>)),
            this, SLOT(ignoredJobsReceived(QSet<QString>)));

        Q_ASSERT (success);
        Q_UNUSED (success);
    }

    emit modelChanged();
}

OdJobsListModel *
OdJobsProxyModel::model () const
{
    return m_sourceModel;
}

void
OdJobsProxyModel::ignoredJobsReceived (
        const QSet<QString> ids)
{
    m_disabledIds += ids;
    invalidateFilter();
}

void
OdJobsProxyModel::appendDisabledJobId (
        const QString &id)
{
    SYS_DEBUG ("*** id: '%s'", SYS_STR(id));
    if (m_sourceModel)
        m_sourceModel->appendDisabledJobId(id);

    m_disabledIds.insert(id);
    invalidateFilter();
}

bool
OdJobsProxyModel::filterAcceptsRow (
        int                source_row,
        const QModelIndex &source_parent) const
{
    QString      title;
    QString      description;
    QString      skills;
    QString      searchString;

    Q_UNUSED(source_parent);
    QModelIndex  idx;

    // The diabled jobs, a list of jobs that we want to hide. The list comes
    // from the SQL database.
    idx = m_sourceModel->index (source_row, 0);
    if (m_disabledIds.contains(idx.data(OdJobsListModel::JobRecno).toString()))
        return false;

    title = idx.data (OdJobsListModel::JobTitleRole).
            toString().toLower();
    description = idx.data (OdJobsListModel::JobDescriptionRole).
            toString().toLower();
    skills = idx.data (OdJobsListModel::JobReqSkillsRole).
            toString().toLower();

    // The server sometimes does a stupid string. You enter the search
    // string as 'c++' and what it sends is actually matches for 'c'. We do the
    // filtering here, az the client side better.
    searchString = m_sourceModel->search().toLower();
    if (!searchString.isEmpty()) {
        if (!title.contains(searchString) &&
                !description.contains(searchString) &&
                !skills.contains(searchString))
            return false;
    }

    if (!m_filtersEnabled)
        return true;

    if (filterIsOn(FilterNotPhpCowboy)) {
        if (idx.data(OdJobsListModel::JobIsWebRelated).toBool())
            return false;
    }

    if (filterIsOn(FilterNoiPhone)) {
        QStringList forbiddenWords;

        forbiddenWords << "iphone" << "ios" << "ipad";

        foreach (const QString &fWord, forbiddenWords) {
            if (title.contains(fWord) ||
                    description.contains(fWord) ||
                    skills.contains(fWord)) {
                //SYS_WARNING ("*** filtered: %s", SYS_STR(title));
                return false;
            }
        }
    }

    if (filterIsOn(FilterNoAndroid)) {
        QStringList forbiddenWords;

        forbiddenWords << "android";

        foreach (const QString &fWord, forbiddenWords) {
            if (title.contains(fWord) ||
                    description.contains(fWord) ||
                    skills.contains(fWord)) {
                //SYS_WARNING ("*** filtered: %s", SYS_STR(title));
                return false;
            }
        }
    }

    if (filterIsOn(FilterNoGreyComputing)) {
        QStringList forbiddenWords;

        forbiddenWords << "crawl" << "scrape" <<
            "address verifier" << "cold calling" <<
            "adult content" << "seo";

        foreach (const QString &fWord, forbiddenWords) {
            if (title.contains(fWord) ||
                    description.contains(fWord) ||
                    skills.contains(fWord)) {
                //SYS_WARNING ("*** filtered: %s", SYS_STR(title));
                return false;
            }
        }
    }

    if (filterIsOn(FilterNotForPeanuts)) {
        if (idx.data(OdJobsListModel::JobIsCheap).toBool())
            return false;
    }

    return true;
}



bool
OdJobsProxyModel::filterIsOn (
        const FilterType filter) const
{
    return m_filtersOnOff[filter];
}

void
OdJobsProxyModel::setFilterIsOn (
        const FilterType filter,
        const bool       on)
{
    m_filtersOnOff[filter] = on;
}
