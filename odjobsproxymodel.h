#ifndef ODJOBSPROXYMODEL_H
#define ODJOBSPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QPointer>
#include <QHash>
#include <QSet>

#include "odjobslistmodel.h"

class OdJobsProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY (
            OdJobsListModel *sourceModel
            READ model
            WRITE setModel
            NOTIFY modelChanged)

    Q_PROPERTY (
            bool filtersEnabled
            READ filtersEnabled
            WRITE setFiltersEnabled
            NOTIFY filtersEnabledChanged)

    Q_PROPERTY (
            bool notForPeanuts
            READ notForPeanuts
            WRITE setNotForPeanuts
            NOTIFY notForPeanutsChanged)

    Q_PROPERTY (
            bool notPhpCowboy
            READ notPhpCowboy
            WRITE setNotPhpCowboy
            NOTIFY notPhpCowboyChanged)

    Q_PROPERTY (
            bool noiPhone
            READ noiPhone
            WRITE setNoiPhone
            NOTIFY noiPhoneChanged)

    Q_PROPERTY (
            bool noAndroid
            READ noAndroid
            WRITE setNoAndroid
            NOTIFY noAndroidChanged)

    Q_PROPERTY (
            bool noGreyComputing
            READ noGreyComputing
            WRITE setNoGreyComputing
            NOTIFY noGreyComputingChanged)

    public:
        enum FilterType {
            FilterNotForPeanuts,
            FilterOnlyForPolite,
            FilterNotPhpCowboy,
            FilterNoGreyComputing,
            FilterNoAndroid,
            FilterNoiPhone,
            LastFilter
        };
        explicit OdJobsProxyModel(QObject *parent = 0);

        void setModel (OdJobsListModel *model);
        OdJobsListModel *model () const;

        void setFiltersEnabled (bool value);
        bool filtersEnabled () const;

        void setNotForPeanuts (bool value);
        bool notForPeanuts () const;

        void setNotPhpCowboy (bool value);
        bool notPhpCowboy () const;

        void setNoiPhone (bool value);
        bool noiPhone () const;

        void setNoAndroid (bool value);
        bool noAndroid () const;

        void setNoGreyComputing (bool value);
        bool noGreyComputing () const;

        Q_INVOKABLE void appendDisabledJobId (const QString &id);

    protected:
        virtual bool filterAcceptsRow (
                int                source_row,
                const QModelIndex &source_parent ) const;

        bool filterIsOn (const FilterType filter) const;
        void setFilterIsOn (
                const FilterType filter,
                const bool       on);

    signals:
        void modelChanged ();
        void filtersEnabledChanged ();
        void notForPeanutsChanged ();
        void notPhpCowboyChanged ();
        void noiPhoneChanged ();
        void noAndroidChanged ();
        void noGreyComputingChanged ();

    public slots:
        void ignoredJobsReceived (const QSet<QString> ids);

    private:
        QPointer<OdJobsListModel> m_sourceModel;
        QHash<FilterType, bool>   m_filtersOnOff;
        bool                      m_filtersEnabled;
        QSet<QString>             m_disabledIds;
};

#endif
