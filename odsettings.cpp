#include "odsettings.h"

#define DEBUG
#include "debug.h"

OdSettings *OdSettings::sm_instance;

OdSettings::OdSettings(QObject *parent) :
    QObject(parent),
    m_settings ("BorgCollective", "odeskqml")
{
    sm_instance = this;
}

OdSettings *
OdSettings::instance ()
{
    if (!sm_instance)
        sm_instance = new OdSettings ();

    return sm_instance;
}

void
OdSettings::setBoolean (
        const QString &key,
        const bool     value)
{
    m_settings.setValue (key, value);
}

bool
OdSettings::getBoolean (
        const QString &key,
        const bool     defaultValue)
{
    return m_settings.value (key, defaultValue).toBool();
}

void
OdSettings::setString (
        const QString &key,
        const QString &value)
{
    m_settings.setValue(key, value);
}

QString
OdSettings::getString (
        const QString &key,
        const QString  defaultValue)
{
    return m_settings.value (key, defaultValue).toString();
}
