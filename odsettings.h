#ifndef ODSETTINGS_H
#define ODSETTINGS_H

#include <QObject>
#include <QSettings>

#define SAVEBOOL(key,value) \
    OdSettings::instance()->setBoolean(key, value);

#define LOADBOOL(key,defaultValue) \
    OdSettings::instance()->getBoolean(key, defaultValue)

class OdSettings : public QObject
{
    Q_OBJECT
    public:
        static OdSettings *instance ();

        void setBoolean (
                const QString &key,
                const bool     value);

        bool getBoolean (
                const QString &key,
                const bool     defaultValue = false);

        void setString (
                const QString &key,
                const QString &value);

        QString getString (
                const QString &key,
                const QString  defaultValue = QString());

    signals:
    
    public slots:

    private:
        OdSettings(QObject *parent = 0);

    private:
        static OdSettings   *sm_instance;
        QSettings             m_settings;

};

#endif
