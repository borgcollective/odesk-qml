#include "odsqldatabase.h"

#include <QSqlError>
#include <QStringList>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QHostInfo>
#include <QHostAddress>

#define DEBUG
#define WARNING
#include "debug.h"

OdSqlDatabase::OdSqlDatabase(QObject *parent) :
    QObject(parent)
{
    qRegisterMetaType<OdSqlDatabase::OdSqlStatus> (
                "OdSqlDatabase::OdSqlStatus");
}

OdSqlDatabase::~OdSqlDatabase()
{
    storeStatus (false);
}

void
OdSqlDatabase::setDatabaseInfo (
        const QString serverName,
        const QString databaseName,
        const QString userName,
        const QString password)
{
    m_serverName = serverName;
    m_databaseName = databaseName;
    m_userName = userName;
    m_password = password;
}

void
OdSqlDatabase::disconnectFromDatabase()
{
    m_database.close();
    emit statusChanged(SqlStatusDisconnected);
}

void
OdSqlDatabase::connectToDatabase()
{
    QString         driverName = "QPSQL";
    bool            success;

    SYS_DEBUG ("Connecting to database.");
    emit statusChanged(SqlStatusConnecting);
    m_database = QSqlDatabase::addDatabase (driverName, "xpns");
    m_database.setDatabaseName (m_databaseName);
    m_database.setUserName (m_userName);
    m_database.setPassword (m_password);
    m_database.setHostName (m_serverName);

    success = m_database.open();
    if (!success) {
        QSqlError error = m_database.lastError ();
        SYS_WARNING ("SQL: %s", SYS_STR(error.text()));
        emit statusChanged(SqlStatusConnectError);
        return;
    } else {
        SYS_DEBUG ("Connected to database");
        emit statusChanged (SqlStatusConnected);
    }

    QStringList sqlTables;
    sqlTables = m_database.tables();
    SYS_DEBUG ("Have %d tables: %s",
               sqlTables.size(), SYS_STR(sqlTables.join(";")));
    if (!sqlTables.contains("jobs")) {
        createTables();
    }

    m_setJobStatus = QSqlQuery (m_database);
    m_setJobStatus.prepare (
        "SELECT * FROM setjobstatus (:jobid, :userid, :status);");

    m_insertUserStatus = QSqlQuery (m_database);
    m_insertUserStatus.prepare (
        "INSERT INTO userstatus (userid, status, host)"
        "  VALUES (:userid, :status, :host);");

    m_deleteUserStatus = QSqlQuery (m_database);
    m_deleteUserStatus.prepare (
        "DELETE FROM userstatus"
        "  WHERE userid = :userid AND host = :host;");

    storeStatus (true);
    readIgnoredJobs ();
    emit statusChanged (SqlStatusConnected);
}

void
OdSqlDatabase::readIgnoredJobs()
{
    QSet<QString> ids;
    QSqlQuery     query(m_database);
    int           fieldNo;

    query.prepare(
        "SELECT jobid FROM jobs"
        "  WHERE status = 'ignored';");
    query.exec ();
    QSqlError error = m_database.lastError ();
    if (error.isValid()) {
        SYS_WARNING ("SQL: ERROR: %s", SYS_STR(error.text()));
        emit statusChanged(SqlStatusConnectError);
    }

    fieldNo = query.record().indexOf("jobid");
    while (query.next()) {
        QString jobId = query.value(fieldNo).toString();
        ids.insert(jobId);
    }

    SYS_DEBUG ("*** loaded %d ignored jobs", ids.size());
    emit ignoredJobsReceived(ids);
}

void
OdSqlDatabase::addIgnoredJob (
        const QString jobId)
{
    m_setJobStatus.bindValue (":jobid", jobId);
    m_setJobStatus.bindValue (":userid", m_userName);
    m_setJobStatus.bindValue (":status", "ignored");
    if (!m_setJobStatus.exec()) {
        QSqlError error = m_database.lastError ();
        SYS_WARNING ("SQL: ERROR: %s", SYS_STR(error.text()));
        emit statusChanged(SqlStatusConnectError);
    }
}

void
OdSqlDatabase::storeStatus (
        const bool onLine)
{
    QHostInfo info;
    QString   host = info.localHostName();
    //QString   ipString;
    QHostInfo info2 (QHostInfo::fromName(host));
    QList<QHostAddress> addressList = info2.addresses();

    SYS_DEBUG ("*** storing status, online = %s", SYS_BOOL(onLine));
    foreach (const QHostAddress &addr, addressList) {
        if (addr.protocol() == QAbstractSocket::IPv4Protocol) {
            host = addr.toString();
            break;
        }
    }

    m_database.transaction();
    m_deleteUserStatus.bindValue (":userId", m_userName);
    m_deleteUserStatus.bindValue (":host", host);
    if (!m_deleteUserStatus.exec()) {
        SYS_WARNING ("SQL: Error deleting");
        emit statusChanged(SqlStatusConnectError);
        m_database.rollback();
    }

    if (onLine) {
        m_insertUserStatus.bindValue (":userId", m_userName);
        m_insertUserStatus.bindValue (":status", "on-line");
        m_insertUserStatus.bindValue (":host", host);
        if (!m_insertUserStatus.exec()) {
            SYS_WARNING ("SQL: Error inserting");
            emit statusChanged(SqlStatusConnectError);
            m_database.rollback();
        }
    }

    m_database.commit();
}

void
OdSqlDatabase::createTables()
{
    SYS_DEBUG ("Creating databases");
    QStringList createCommands;

    createCommands <<
        "CREATE TABLE jobs ("
        "  jobid     varchar(32) NOT NULL,"
        "  userid    varchar(32) NOT NULL,"
        "  status    varchar(16) NOT NULL,"
        "  created   timestamp default now()"
        ");" <<
        "CREATE TABLE userstatus ("
        "  userid    varchar(32) NOT NULL,"
        "  status    varchar(8)  NOT NULL,"
        "  host      varchar(32) NOT NULL,"
        "  last_seen timestamp default now()"
        ");" <<
        "CREATE TABLE timestamps ("
        "  category  varchar(16) not NULL,"
        "  time      timestamp default now()"
        ");" <</*
        "CREATE TABLE newsData ("
        "  from      varchar(16) NOT NULL,"
        "  time      timestamp default now(),"
        "  message   text NOT NULL"
        ");" <<*/
        "INSERT INTO timestamps VALUES ('jobs');" <<
        "INSERT INTO timestamps VALUES ('userstatus');" <<
        "create or replace function setjobstatus(varchar(32),varchar(32),varchar(16)) "
        "returns boolean as $$ "
        "declare "
        "  _jobid alias for $1; "
        "  _userid alias for $2; "
        "  _status alias for $3; "
        "  begin "
        "    update jobs set status=_status where jobid=_jobid and userid=_userid; "
        "    if not found then "
        "      insert into jobs (jobid,userid,status) values (_jobid,_userid,_status); "
        "    end if; "
        "  return true; "
        "end; "
        "$$ language plpgsql;"
        ;


    foreach (const QString &command, createCommands) {
        executeCommand(command);
    }
}

bool
OdSqlDatabase::executeCommand (
        const QString &command)
{
    m_database.exec (command);

    QSqlError error = m_database.lastError ();
    bool      retval (false);

    if (error.isValid()) {
        SYS_WARNING ("SQL: ERROR: %s", SYS_STR(error.text()));
        emit statusChanged(SqlStatusConnectError);
    } else {
        retval = true;
    }

    return retval;
}
