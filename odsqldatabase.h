#ifndef ODSQLDATABASE_H
#define ODSQLDATABASE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSet>
#include <QString>

class OdSqlDatabase : public QObject
{
    Q_OBJECT
    Q_ENUMS(OdSqlStatus)

    public:
        enum OdSqlStatus {
            SqlStatusUnknown,
            SqlStatusConnecting,
            SqlStatusConnected,
            SqlStatusDisconnected,
            SqlStatusConnectError
        };

        OdSqlDatabase (QObject *parent = 0);
        ~OdSqlDatabase ();
    
    signals:
        void ignoredJobsReceived (const QSet<QString> ids);
        void statusChanged (OdSqlDatabase::OdSqlStatus status);

    public slots:
        void setDatabaseInfo (
                const QString serverName,
                const QString databaseName,
                const QString userName,
                const QString password);
        void connectToDatabase ();
        void disconnectFromDatabase ();
        void addIgnoredJob (const QString jobId);
        void storeStatus (const bool onLine);

    private:
        void readIgnoredJobs ();
        void createTables ();
        bool executeCommand (const QString &command);

    private:
        QSqlDatabase   m_database;
        QString        m_serverName;
        QString        m_databaseName;
        QString        m_userName;
        QString        m_password;

        QSqlQuery      m_setJobStatus;
        QSqlQuery      m_insertUserStatus;
        QSqlQuery      m_deleteUserStatus;
};

//Q_DECLARE_METATYPE(OdSqlDatabase::OdSqlStatus);

#endif
