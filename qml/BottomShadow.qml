import QtQuick 1.1

Rectangle {
    id             : root
    anchors.left   : parent.left
    anchors.right  : parent.right
    height         : 8
    opacity        : 0.5

    gradient: Gradient {
        GradientStop { position: 0.0; color: "#000000" }
        GradientStop { position: 0.2; color: "#222222" }
        GradientStop { position: 1.0; color: "transparent" }
    }
}

