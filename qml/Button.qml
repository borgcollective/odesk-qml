import QtQuick 1.1
import "theme.js" as Theme

FocusScope {
    id      : button
    width   : label.width + 2 * Theme.mediumMargin
    height  : label.height + 2 * Theme.mediumMargin
    opacity : enabled ? 1.0 : 0.5

    property alias radius: background.radius
    property alias text: label.text
    property alias pressed: mouseArea.pressed
    signal clicked()

    property color fontColor: "Black"
    property color highColor: "White"
    property color lowColor: "#dddddd"
    property color lineColor: "#bbbbbb"

    Rectangle {
        id           : background
        anchors.fill : parent
        border.color : lineColor
        radius       : Theme.normalRadius
        smooth       : true

        SystemPalette { id: palette }

        gradient: mouseArea.pressed ? pressedGradient : inactiveGradient
        
        Gradient {
            id: inactiveGradient
            GradientStop { position: 0.0; color: highColor }
            GradientStop { position: 1.0; color: lowColor }
        }

        Gradient {
            id: pressedGradient
            GradientStop { position: 0.0; color: lowColor }
            GradientStop { position: 1.0; color: highColor }
        }

        Text {
            id               : label
            anchors.centerIn : parent
            font.pixelSize   : 18
            font.family      : Theme.fontFamily
            color            : button.fontColor
            textFormat       : Text.PlainText
            smooth           : true
        }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            focus: true;
            onClicked: button.clicked()
        }

        Keys.onSpacePressed:  keyboardClick()
        Keys.onEnterPressed:  keyboardClick()
        Keys.onReturnPressed: keyboardClick()

        function keyboardClick() {
            button.clicked();
            gradient = pressedGradient;
            resetGradient.start();
        }
    }
}
