import QtQuick 1.1
import "theme.js" as Theme

Item {
    id            : root
    height        : content.height + 2 * Theme.normalMargin
    anchors.left  : parent.left
    anchors.right : parent.right
    anchors.margins : Theme.normalMargin

    property alias title: titleText.text
    property bool checked: false
    property bool sensitive:  true

    signal clicked ()

    Row {
        id            : content
        anchors.left  : parent.left
        anchors.right : parent.right
        anchors.verticalCenter: parent.verticalCenter
        height        : Math.max (titleText.height, checkmark.height);
        spacing       : Theme.mediumMargin
        opacity       : root.sensitive ? 1.0 : 0.3

        Image {
            id            : rectangle
            height        : checkmark.height + 8
            width         : checkmark.width + 8
            anchors.verticalCenter: parent.verticalCenter
            source        : "pixmaps/checkbox-background.png"
            smooth        : true

            Image {
                id               : checkmark
                width            : 25
                height           : width
                source           : "pixmaps/tickmark.png"
                anchors.centerIn : parent
                opacity          : checked ? 1.0 : 0.0
                smooth           : true
                Behavior on opacity {
                    NumberAnimation {
                        duration: 200
                        easing.type: Easing.Linear
                    }
                }
            }
        }
        
        Text {
            id              : titleText
            font.pixelSize  : 18
            width           : parent.width - rectangle.width 
            height          : rectangle.height
            //font.bold       : true
            elide           : Text.ElideRight
            textFormat      : Text.PlainText
            verticalAlignment: Text.AlignVCenter
            text            : root.title
            smooth          : true
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 200
                easing.type: Easing.Linear
            }
        }
    }

    MouseArea {
        enabled: true
        anchors.fill: parent
        onClicked: {
            if (root.sensitive)
                root.clicked()
        }
    }
}
