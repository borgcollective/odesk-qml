import QtQuick 1.1
 
Rectangle {
    property int posX: 0
    property int posY: 0
    property int size: 200
 
    width   : size
    height  : size
    radius  : size / 2
}
