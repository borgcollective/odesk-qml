import QtQuick 1.1
import "theme.js" as Theme

Item {
    id            : root
    anchors.fill  : parent
    property int minWidth: 400
    property int minHeight: 200
    property bool okButtonEnabled: true
    property alias title: titleLabel.text

    default property alias children : clientArea.children

    signal okButtonClicked ()
    signal cancelButtonClicked ()

    Rectangle {
        id          : windowShader
        anchors.fill: parent
        color       : "Black"
        opacity     : 0.4

        MouseArea {
            anchors.fill: parent
            onClicked : {
                console.log ("Dialog event eater clicked");
                //root.cancelButtonClicked ();
            }
        }
    }

    Rectangle {
        id          : dialogContentShadow
        color       : "black"
        width       : dialogContent.width
        height      : dialogContent.height
        x           : dialogContent.x + 10
        y           : dialogContent.y + 10
        radius      : dialogContent.radius
        smooth      : true
        opacity     : 0.3

    }

    Rectangle {
        id          : dialogContent
        anchors.centerIn: parent
        width       : minWidth
        height      : minHeight + titleLabel.height + titleLine.height
        color       : "White"
        radius      : 5
        smooth      : true
        opacity     : 0.9

        Text {
            id               : titleLabel
            color            : "Black"
            anchors.right    : parent.right
            anchors.top      : parent.top
            anchors.margins  : Theme.normalMargin
            font.pixelSize   : 32
            //font.bold        : true
            smooth           : true
        }

        Rectangle {
            id              : titleLine
            height          : 5
            color           : "#333333"
            anchors.leftMargin: Theme.normalMargin
            anchors.rightMargin: Theme.normalMargin
            anchors.left    : parent.left
            anchors.right   : parent.right
            anchors.top     : titleLabel.bottom
        }

        Item {
            id              : clientArea
            anchors.margins : Theme.normalMargin
            anchors.top     : titleLine.bottom
            anchors.bottom  : buttonArea.top
            anchors.left    : parent.left
            anchors.right   : parent.right
        }

        Item {
            id             : buttonArea
            width          : parent.width
            height         : button1.height + 2 * Theme.normalMargin
            anchors.bottom : parent.bottom

            Button {
                id : button1
                anchors.right: button2.left
                anchors.bottom: parent.bottom
                anchors.margins : Theme.normalMargin
                text            : "Cancel"

                onClicked : {
                    root.cancelButtonClicked ();
                    //activity.userAnswer = 0
                    //activity.active     = false
                }
            }

            GreenButton {
                id : button2
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins : Theme.normalMargin
                text            : "Sign in"
                opacity         : root.okButtonEnabled ? 1.0 : 0.4
                enabled         : root.okButtonEnabled
                onClicked : {
                    root.okButtonClicked ()
                    //activity.userAnswer = 1
                    //activity.active     = false
                }
            }
        }
    }

    Behavior on opacity {
        NumberAnimation {
            duration: 200
        }
    }
}
