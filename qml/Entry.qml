import QtQuick 1.1
import "theme.js" as Theme

Item {
    id             : root
    height         : 40
    anchors.margins: Theme.normalMargin

    property bool focused: false
    property bool passwordMode: false

    property alias text: textInput.text
    property alias prompt: promptLabel.text
    property alias textColor: textInput.color

    signal accepted ()

    MouseArea {
        anchors.fill: parent
        onPressed : {
            textInput.focus = true
        }
    }
    
    Rectangle {
        anchors.fill: parent
        color       : "White"
        border.width : Theme.normalBorderWidth
        border.color : focused ? Theme.oDeskBlue : "#d2d2cd"
        radius       : Theme.normalRadius
        smooth       : true 
    }

    
    TextInput {
        property bool focused: focus

        id               : textInput
        echoMode         : passwordMode ? TextInput.Password : TextInput.Normal
        font.pixelSize   : 18
        font.family      : Theme.fontFamily

        anchors.verticalCenter: parent.verticalCenter
        anchors.left     : parent.left
        anchors.right    : parent.right
        anchors.margins  : Theme.normalMargin
        inputMethodHints : Qt.ImhPreferLowercase | Qt.ImhNoPredictiveText
        activeFocusOnPress : true
        smooth           : true

        onActiveFocusChanged: {
            root.focused = focus 
            if (activeFocus) {
                // cancels error message
                //errorMessage = ""
            }
        }

        Keys.onEnterPressed : {
            //focus = false
            root.accepted ();
        }
        
        Keys.onReturnPressed : {
            //focus = false
            root.accepted ();
        }
    }

    Text {
        id               : promptLabel
        anchors.left     : parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins  : Theme.normalMargin
        color            : "Grey"
        font.pixelSize   : 18
        smooth           : true
        visible          : root.text == ""
    }
}
