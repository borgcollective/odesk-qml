import QtQuick 1.1
import "theme.js" as Theme

Flickable {
    id             : root
    anchors.left   : parent.left
    anchors.right  : parent.right
    contentHeight  : label.height
    clip           : true

    property alias text: label.text

    Text {
        id               : label
        width            : parent.width
        font.pointSize   : 10
        wrapMode         : Text.WordWrap
        smooth           : true
        font.family      : Theme.fontFamily
    }
}
