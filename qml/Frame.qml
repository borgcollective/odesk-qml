import QtQuick 1.1
import "theme.js" as Theme

Rectangle {
    id            : root
    width         : parent.width 
    border.color  : Theme.darkGrey
    border.width  : Theme.thinBorderWidth
    radius        : Theme.normalRadius
    smooth        : true
}

