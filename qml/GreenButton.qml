import QtQuick 1.1
import "theme.js" as Theme

Button {
    fontColor     : "White"
    highColor     : Theme.greenHColor
    lowColor      : Theme.greenLColor
    lineColor     : Theme.greenFColor
}
