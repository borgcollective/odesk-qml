import QtQuick 1.1
import "theme.js" as Theme
import oDeskBackend 1.0

Item {
    id      : root 
    // Damn borders are always outside
    height  : background.height + 5
    width   : parent.width
    clip    : true

    property string jobId       : ""
    property alias title        : jobTitle.text
    property alias description  : descriptionText.text
    property string engagement  : ""
    property string type: ""
    property string start: ""
    property string skills: ""
    property int amount: 0
    property int hourlyRate: 0
    property int nItems: 0

    property real detailsOpacity : 0
    signal activated()

    onNItemsChanged: {
        state = ""
    }

    Rectangle {
        id              : background 
        anchors.left    : parent.left
        anchors.right   : parent.right
        height          : jobTitle.height + jobStart.height +
            engagementLabel.height + 2 * Theme.normalMargin
        border.width    : Theme.thinBorderWidth
        radius          : 3
        border.color    : Theme.greyFColor
        smooth          : true

        gradient: {
            if (root.state != "Details") 
                return mouseArea.pressed ? pressedGradient : inactiveGradient

            return detailsGradient
        }

        Gradient {
            id: inactiveGradient
            GradientStop { position: 0.0; color: Theme.greyHColor }
            GradientStop { position: 1.0; color: Theme.greyLColor }
        }

        Gradient {
            id: pressedGradient
            GradientStop { position: 0.0; color: Theme.greyLColor }
            GradientStop { position: 1.0; color: Theme.greyHColor }
        }
        
        Gradient {
            id: detailsGradient
            GradientStop { position: 0.0; color: Theme.greyHColor }
            GradientStop { position: 1.0; color: Theme.greyHColor }
        }

        Row {
            id            : briefContent
            anchors.left  : parent.left
            anchors.right : parent.right
            anchors.top   : parent.top
            anchors.margins: Theme.normalMargin
            spacing       : Theme.normalMargin
            height        : jobTitle.height + jobStart.height +
                engagementLabel.height

            Image {
                id              : icon
                height          : parent.height
                width           : height
                smooth          : true
                source : "pixmaps/" + model.jobIconName + ".png"

                MouseArea {
                    anchors.fill: parent
                    enabled: root.state == "Details"
                    onClicked: {
                        console.log ("Icon clicked")
                        root.activated ();
                    }
                }
            }

            Column {
                id: briefLabels
                width: parent.width - icon.width - 3 * Theme.normalMargin
                Text {
                    id               : jobTitle
                    width            : parent.width
                    wrapMode         : Text.WordWrap
                    textFormat       : Text.PlainText
                    elide            : Text.ElideRight
                    font.pixelSize   : root.state == "Details" ? 20 : 18
                    font.family      : Theme.fontFamily
                    color            : Theme.oDeskBlue
                    smooth           : true
                }

                Text {
                    id               : jobStart
                    wrapMode         : Text.WrapAnywhere
                    font.family      : Theme.fontFamily
                    text             : "Start date: " + root.start
                    smooth           : true
                }
            
                Text {
                    id               : engagementLabel
                    color            : "#7c7c7a"
                    font.family      : Theme.fontFamily
                    wrapMode         : Text.WrapAnywhere
                    text : {
                        if (engagement == "")
                            return "<b>" + type + "</b>";
                    
                        return "<b>" + type + "</b> (" + engagement + ")"
                    }
                }

                Text {
                    id               : skillsLabel
                    width            : parent.width
                    font.family      : Theme.fontFamily
                    text             : "Skills: " + root.skills
                    wrapMode         : Text.WrapAnywhere
                    opacity          : detailsOpacity
                }
            }
        }
            
        Text {
            id               : amountLabel
            visible          : text != ""
            anchors.right    : parent.right
            anchors.bottom   : briefContent.bottom
            anchors.leftMargin : Theme.normalMargin
            anchors.rightMargin : Theme.normalMargin
            font.family      : Theme.fontFamily
            color            : Theme.redHColor
            wrapMode         : Text.WrapAnywhere

            text : {
                if (amount > 0)
                    return "$" + amount;

                if (hourlyRate > 0)
                    return "$" + hourlyRate + "/hrs";

                return "";
            }
        }
            
        Text {
            id               : descriptionTitleLabel
            text             : "Job Description"
            font.pixelSize   : Theme.subTitleFontSize
            font.family      : Theme.fontFamily
            anchors.left     : parent.left
            anchors.right    : parent.right
            opacity          : detailsOpacity
            anchors.top      : briefContent.bottom
            anchors.margins  : Theme.normalMargin
            anchors.topMargin : Theme.mediumMargin
            smooth           : true
        }

        FlickableText {
            id               : descriptionText
            anchors.top      : descriptionTitleLabel.bottom
            anchors.bottom   : closeButton.top
            opacity          : detailsOpacity
            anchors.margins  : Theme.normalMargin
        }

            Button {
                id             : closeButton
                text           : "Close"
                anchors.bottom : parent.bottom
                anchors.right  : parent.right
                anchors.margins : Theme.normalMargin
                opacity        : detailsOpacity 
                
                onClicked : {
                    root.state = ""
                }
            }

            GreenButton {
                id             : applyButton
                text           : "Apply to this Job"
                width          : 160
                anchors.right  : closeButton.left
                anchors.bottom : parent.bottom
                anchors.margins : Theme.normalMargin
                opacity        : detailsOpacity 

                onClicked : {
                    Qt.openUrlExternally (
                        "https://www.odesk.com/job/" +
                        root.jobId +
                        "/apply/");
                }
            }
    }

    MouseArea {
        id           : mouseArea
        anchors.fill : parent
        enabled      : root.state != "Details"
        onClicked: { 
            root.state = "Details"
            backend.loadJobDetails(root.jobId)
        }
    }
    
    states: State {
        name: "Details"
        PropertyChanges { target: background; color: "white" }
        PropertyChanges { target: jobTitle; maximumLineCount: 3 }
        PropertyChanges { target: root; detailsOpacity: 1; x: 0 } 

        PropertyChanges { target: background; height: view.height - 2 * Theme.normalMargin}
        PropertyChanges { target: background; y: Theme.normalMargin}

        //PropertyChanges { target: background; anchors.top: view.top }
        //PropertyChanges { target: background; anchors.bottom: view.bottom }

        PropertyChanges { target: root.ListView.view; explicit: true; contentY: root.y }
        PropertyChanges { target: root.ListView.view; interactive: false }
        PropertyChanges { target: briefContent; height: 120 }
    }

    transitions: Transition {
        // Make the state changes smooth
        ParallelAnimation {
            NumberAnimation { 
                duration: Theme.normalAnimationLength
                properties: "height,detailsOpacity,contentY" 
            }
        }
    }

    ListView.onAdd: SequentialAnimation {
        PropertyAction { 
            target: root; 
            property: "scale"; 
            value: 0.0
        }

        NumberAnimation { 
            target      : root
            property    : "scale"
            to          : 1.0 
            duration    : 250 
            easing.type : Easing.InOutQuad 
        }
    }

    ListView.onRemove: SequentialAnimation {
        PropertyAction { 
            target: root; 
            property: "ListView.delayRemove"; 
            value: true 
        }
    
        NumberAnimation { 
            target   : root 
            property : "scale" 
            to       : 0.0
            duration : 250
            easing.type: Easing.InOutQuad 
        }

        PropertyAction { 
            target: root; 
            property: "ListView.delayRemove"; 
            value: false 
        }
    }
}

