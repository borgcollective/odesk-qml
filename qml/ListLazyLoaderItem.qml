import QtQuick 1.1
import "theme.js" as Theme
import oDeskBackend 1.0

Item {
    height        : content.height

    property variant model

    Column {
        id            : content
        anchors.left  : parent.left
        anchors.right : parent.right
        height : 100
        Item {
            height: Theme.listItemSpacing
            anchors.left  : parent.left
            anchors.right : parent.right
        }

        Button {
            text          : "Load more"
            anchors.left  : parent.left
            anchors.right : parent.right
            onClicked : {
                console.log ("Not implemnted")
            }
        }

        Item {
            height: Theme.listItemSpacing
            anchors.left  : parent.left
            anchors.right : parent.right
        }
    }
}
