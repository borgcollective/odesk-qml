import QtQuick 1.1
import "theme.js" as Theme
import oDeskBackend 1.0

Rectangle {
    id             : root
    anchors.margins: Theme.normalMargin

    property alias loginName: loginNameEntry.text
    property alias password: passwordEntry.text

    signal accepted ()

        Column {
            id            : content
            anchors.top   : parent.top
            anchors.left  : parent.left
            anchors.right : parent.right
            spacing       : Theme.mediumMargin
            anchors.margins : Theme.normalMargin

            Entry {
                id             : loginNameEntry
                anchors.left   : parent.left
                anchors.right  : parent.right
                prompt         : "Username"
                text           : backend.sqlUserName
                //focus: true
            }

            Entry {
                id             : passwordEntry
                anchors.left   : parent.left
                anchors.right  : parent.right
                prompt         : "Password"
                text           : backend.sqlPassword
                passwordMode   : true
            }

            CheckButton {
                title: "Remember me"
                anchors.right: parent.right
                anchors.left : parent.left
                // TBD
                checked      : true
                enabled      : false
                opacity      : 0.2
                onClicked : {
                    checked = !checked
                }
            }
        }

}
