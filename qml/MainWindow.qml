import QtQuick 1.1
import "theme.js" as Theme
import oDeskBackend 1.0

Rectangle {
    id     : root
    width  : 480
    height : 600
    state  : backend.stateName
    
    property string activeId: ""

    TopMenu {
        id            : header
        anchors.left  : parent.left
        anchors.right : parent.right
        anchors.top   : parent.top
        color         : Theme.oDeskBlue
        nJobs         : searchPage.nJobs
        currentPage   : pager.currentIndex

        onFindworkActivated: {
            pager.currentIndex = 0
        }

        onSettingsActivated: {
            pager.currentIndex = 1
        }
    }

    Pager {
        id             : pager
        anchors.left   : parent.left
        anchors.right  : parent.right
        anchors.top    : header.bottom
        anchors.bottom : parent.bottom

        model: VisualItemModel {
            SearchPage {
                id     : searchPage
                width  : root.width
                height : root.height - header.height

                onActivated : {
                    console.log ("*** id: ", id);
                    root.activeId = id
                    root.state    = "popup"
                }
            }

            SettingsPage {
                id     : filterPage
                width  : root.width
                height : root.height - header.height
                model  : searchPage.model
            }
/*
            LoginPage {
                width          : root.width
                height         : root.height
                onAccepted : {
                    console.log ("Accepted...");
                    // Use loginName and password to access data
                }
            }*/
        }
    }

    Item {
        id           : popupDialog
        anchors.fill : parent

        Rectangle {
            opacity      : root.state == "popup" ? 0.3 : 0.0
            anchors.fill : parent
            color        : "Black"
            Behavior on opacity {
                NumberAnimation {
                    duration: 200
                }
            }
        }

        MouseArea {
            id           : eventEater
            enabled      : root.state == "popup"
            anchors.fill : parent
            onClicked: {
                console.log ("event eater clicked")
                root.state = ""
            }
        }

        Button {
            opacity          : root.state == "popup" ? 1.0 : 0.0
            anchors.centerIn : parent
            text             : "Hide Job"

            Behavior on opacity {
                NumberAnimation {
                    duration: 200
                }
            }

            onClicked: {
                console.log ("tbd button: ", activeId)
                searchPage.model.appendDisabledJobId(root.activeId);
                root.state = ""
            }
        }
    }

    Dialog {
        opacity: root.state == "sqllogindialog" ? 1.0 : 0.0
        title: "Log in to workgroup"
        width: 400
        height: 300
        LoginPage {
            id           : workgoup
            anchors.fill : parent
        }

        onOkButtonClicked: {
            //opacity = 0.0
            console.log ("okbuttonclicked")
            backend.loginSqlServer (workgoup.loginName, workgoup.password);
        }

        onCancelButtonClicked: {
            //opacity = 0.0
            console.log ("cancelbuttonclicked")
            backend.loginSqlServerCancelled()
        }
    }
}

