import QtQuick 1.1

Item {
    id     : root

    property alias model: pagerView.model
    property alias currentIndex: pagerView.currentIndex
    property alias cacheBuffer: pagerView.cacheBuffer

    ListView {
        id                  : pagerView
        preferredHighlightBegin: 0;
        preferredHighlightEnd: 0
        highlightRangeMode  : ListView.StrictlyEnforceRange
        orientation         : ListView.Horizontal
        snapMode            : ListView.SnapOneItem; 
        highlightMoveDuration: 200

        anchors { 
            fill: parent
        }
    }

    Item {
        width          : parent.width; 
        height         : 30
        anchors.bottom : parent.bottom 
        visible        : root.model.count > 1
        opacity        : pagerView.moving ? 0.5 : 0.0
        Row {
            anchors.centerIn: parent
            spacing: 10

            Repeater {
                model: root.model.count

                Circle {
                    size   : 10
                    color  : pagerView.currentIndex == index ? "black" : "grey"

                    MouseArea {
                        width            : 20
                        height           : 20
                        anchors.centerIn : parent
                        onClicked        : pagerView.currentIndex = index
                    }
                }
            }
        }

        Behavior on opacity {
            NumberAnimation {
                duration: pagerView.flicking ? 10 : 600
            }
        }
    }
}
