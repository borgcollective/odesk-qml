import QtQuick 1.0

Item {
    id          : root
    anchors.fill: parent

    Rectangle {
        anchors.fill: parent
        color       : "Black"
        opacity     : 0.4
    }

    Image {
        id: progressIndicator
        source: "pixmaps/busy.png";
        anchors.centerIn: parent

        ParallelAnimation {
            running: root.visible

            NumberAnimation {
                target: progressIndicator
                property: "rotation"
                from: 0
                to: 360
                duration: 1000
                loops: Animation.Infinite
            }

            NumberAnimation {
                target: progressIndicator
                property: "opacity"
                from: 0.0
                to: 1.0
                duration: 500
                loops: 1
            }
        }
    }

    MouseArea {
        id: eventEater
        enabled: root.visible
    }
}
