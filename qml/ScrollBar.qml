import QtQuick 1.1

Rectangle {
    color   : "#aaffaa"
    radius  : 2
    border.width: 1
    border.color: "#88cc88"
    smooth  : true

    property variant flickable: parent
    property bool vertical: true
    property bool hideScrollBarsWhenStopped: true
    property int scrollbarWidth: 8


    function sbOpacity()
    {
        if (!hideScrollBarsWhenStopped) {
            return 0.7;
        }

        return (flickable.flicking || flickable.moving) ? 
            // Do we want to hide scrollbars when the flickable is small?
            //                                     xx    xx
            (vertical ? (height >= parent.height ? 0.7 : 0.7) : 
            (width >= parent.width ?               0.7 : 0.7)) : 0;
    }

    opacity: sbOpacity()

    width: vertical ? 
        scrollbarWidth : 
        flickable.visibleArea.widthRatio * parent.width

    height: vertical ? 
        flickable.visibleArea.heightRatio * parent.height : 
        scrollbarWidth

    x: vertical ? 
        parent.width - width - 2: 
        flickable.visibleArea.xPosition * parent.width

    y: vertical ? 
        flickable.visibleArea.yPosition * parent.height : 
        parent.height - height

    Behavior on opacity { 
        NumberAnimation { 
            duration: 200 
        }
    }
}
