import QtQuick 1.1

Item {
    id             : root
    anchors.left   : parent.left
    anchors.right  : parent.right
    height         : searchEntry.height + 5
    anchors.margins: 5
    
    signal activated(string searchString)

    Entry {
        id             : searchEntry
        anchors.left   : parent.left
        anchors.right  : searchButton.left
        anchors.top    : parent.top
        //height : 40
        prompt         : "Find Job"
    
        onAccepted : {
            root.activated (text);
        }
    }

    GreenButton {
        id            : searchButton
        text          : "Search"
        anchors.right : searchBar.right
        anchors.top   : searchEntry.top
        anchors.bottom: searchEntry.bottom

        onClicked : {
            root.activated (searchEntry.text);
        }
    }
}

