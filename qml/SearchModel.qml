import QtQuick 1.0

XmlListModel {
    id: jobsModel
    property bool loading: jobsModel.status == XmlListModel.Loading

    // this is the search query string
    property string search: ""
    // ;A -> ascending, ;D -> descending
    property string sort: "date_posted;D"
    // Hourly/Fixed set empty for all
    property string jobType: ""
    // comma separated list of skils
    property string skills: ""
    // for paging
    property int page: 1
    property int limit: 20

    onSearchChanged: {
        console.log ("search: ", search)
        reQuery ()
    }

    onSortChanged: reQuery ()
    onJobTypeChanged: reQuery ()
    onSkillsChanged: reQuery ()
    onPageChanged: reQuery ()
    onLimitChanged: reQuery ()

    function reQuery () {
        var link =
            "https://www.odesk.com/api/profiles/v1/search/jobs.xml?" +
            "sort=" + sort

        // syntax: page=SKIP_NR;COUNT
        link += "&page=" + (page-1)*limit + ";" + limit

        if (skills) {
            link += "&qs=" + skills
        }

        if (jobType) { // as this one is optional
            link += "&t=" + jobType
        }

        jobsModel.source = link + "&q=" + search
        console.log ("link: ", jobsModel.source);
    }

    source: ""
    query: "/response/jobs/job"
    XmlRole { name: "op_title"; query: 'op_title/string()' }
    XmlRole { name: "op_start_date"; query: 'op_start_date/string()' }
    XmlRole { name: "op_req_skills"; query: 'op_required_skills/string()' }
    XmlRole { name: "op_description"; query: 'op_description/string()' }
    XmlRole { name: "op_engagement"; query: 'op_engagement/string()' }
    XmlRole { name: "job_type"; query: 'job_type/string()' }
    XmlRole { name: "op_recno"; query: 'op_recno/string()' }
    XmlRole { name: "m_amount"; query: 'amount/string()' }
}

