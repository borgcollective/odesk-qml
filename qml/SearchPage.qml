import QtQuick 1.1
import "theme.js" as Theme
import oDeskBackend 1.0

Rectangle {
    id            : root
    property variant model: backend.jobsProxyModel// proxyModel
    property alias nJobs: view.count

    signal activated (string id)

    SearchBar {
        id             : searchBar
        anchors.left   : parent.left
        anchors.right  : parent.right
        
        onActivated : {
            backend.jobsModel.search = searchString
        }
    }
        
    ListView {
        id            : view
        anchors.left  : parent.left
        anchors.right : parent.right
        anchors.top   : searchBar.bottom
        anchors.bottom: parent.bottom
        anchors.margins: Theme.normalMargin
        // There will be a section header at the top anyway, and this way the
        // list slides under the shadow of the header. Nice this way...
        anchors.topMargin: 0
        model         : root.model
        spacing       : Theme.listItemSpacing
        clip          : true

        section.property: "jobCategory"
        Component {
            id: sectionHeading
            SectionHeader {
                title : section
            }
        }

        section.delegate: sectionHeading

        delegate: ListItemDelegate {
            jobId       : model.jobId
            title       : jobTitle
            start       : op_start_date
            engagement  : op_engagement
            skills      : op_req_skills
            description : op_description
            //description : jobDetailsXml //op_description
            type        : job_type
            amount      : m_amount
            hourlyRate  : jobHourlyRate
            nItems      : view.count

            onActivated : {
                root.activated (jobId)
            }
        }

        /*
        footer: ListLazyLoaderItem {
            anchors.left  : parent.left
            anchors.right : parent.right
        }
        */

        ScrollBar {
        }

        Text {
            text             : "No jobs"
            anchors.centerIn : parent
            font.pixelSize   : 32
            font.family      : Theme.fontFamily
            color            : "grey"
            opacity          : parent.count <= 0 ? 0.5 : 0.0
        }
    }

    ProgressIndicator {
        z: 5
        visible: backend.jobsModel.loading
    }

    BottomShadow {
        anchors.top   : searchBar.bottom
        anchors.topMargin: 5
    }
    /*
    JobsProxyModel {
        id          : proxyModel
        sourceModel : JobsListModel {
            id: model
        }
    }*/
}

