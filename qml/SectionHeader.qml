import QtQuick 1.1
import "theme.js" as Theme

Rectangle {
    id             : root
    anchors.left   : parent.left
    anchors.right  : parent.right
    height         : 50

    property alias title: titleLabel.text

    Text {
        id               : titleLabel
        anchors.left     : parent.left
        anchors.right    : parent.right
        font.pixelSize   : Theme.subTitleFontSize
        font.family      : Theme.fontFamily
        anchors.bottom   : parent.bottom
        anchors.margins: Theme.normalMargin
    }
}
