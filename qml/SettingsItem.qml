import QtQuick 1.1
import "theme.js" as Theme

Frame {
    id              : flickableContent
    height          : frameContent.height + 2 * Theme.normalMargin
    anchors.left    : parent.left
    anchors.right   : parent.right
    anchors.margins : 2

    property alias title     : checkbox.title
    property alias helpText  : help.text
    property alias inValue   : checkbox.checked;
    property bool outValue   : inValue
    property alias sensitive : checkbox.sensitive

    Column {
        id              : frameContent
        anchors.left    : parent.left
        anchors.right   : parent.right
        anchors.top     : parent.top
        anchors.margins : Theme.normalMargin
        spacing         : Theme.mediumMargin
        CheckButton {
            id   : checkbox

            onClicked : {
                outValue = !inValue
            }
        }

        Text {
            id              : help
            anchors.left    : parent.left
            anchors.right   : parent.right
            wrapMode        : Text.WordWrap
            textFormat      : Text.PlainText
            color           : "DarkGrey"
        }
    }
}


