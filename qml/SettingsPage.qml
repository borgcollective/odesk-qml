import QtQuick 1.1
import "theme.js" as Theme
import oDeskBackend 1.0

Rectangle {
    id             : root
    
    property variant model: backend.jobsProxyModel

    Flickable {
        anchors.fill : parent
        anchors.leftMargin: Theme.normalMargin
        anchors.rightMargin: Theme.normalMargin

        contentHeight: flickableContent.height
        clip         : true

        Column {
            id            : flickableContent
            anchors.left  : parent.left
            anchors.right : parent.right
            spacing       : Theme.mediumMargin

            Item {
                height: 30
                width: 10
            }

            SlideButton {
                title           : "Enable workroup login."
                checked         : backend.databaseEnabled

                onClicked : {
                    backend.databaseEnabled = !backend.databaseEnabled
                }
            }

            Frame {
                height: frameContent.height + 2 * Theme.mediumMargin
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: 2

                Column {
                    id  : frameContent
                    spacing: Theme.mediumMargin
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.margins: Theme.mediumMargin

                    Entry {
                        id            : sqlServerName
                        prompt        : "SQL server name"
                        text          : backend.sqlServerName
                        anchors.left  : parent.left
                        anchors.right : parent.right
                    }

                    Entry {
                        id            : sqlDatabaseName
                        prompt        : "Database name"
                        text          : backend.sqlDatabaseName
                        anchors.left  : parent.left
                        anchors.right : parent.right
                    }

                    Button {
                        anchors.right : parent.right
                        text : {
                            if (backend.sqlStatus == SqlDatasase.SqlStatusConnected)
                                return "Disconnect"

                            return "Connect"
                        }

                        enabled : {
                            if (!backend.databaseEnabled)
                                return false

                            if (sqlServerName.text == "" ||
                                    sqlDatabaseName.text == "")
                                return false;

                            return true
                        }

                        onClicked: {
                            if (backend.sqlStatus == SqlDatasase.SqlStatusConnected)
                                backend.logout ()
                            else
                                backend.connectSqlServer (
                                        sqlServerName.text,
                                        sqlDatabaseName.text)
                        }
                    }
                }
            }

            Item {
                height: 30
                width: 10
            }

            SlideButton {
                title          : "Enable intelligent job filters."
                checked        : model.filtersEnabled

                onClicked : {
                    model.filtersEnabled = !model.filtersEnabled
                }
            }

            SettingsItem {
                title     : "No low-budget jobs."
                helpText  : "Fixed-price I jobs with budgets lower than $25 " +
                "will be filtered out. Hourly jobs with hourly rates $15 or " +
                "less will also be filtered out."
                sensitive : model.filtersEnabled
                inValue   : model.notForPeanuts
                onOutValueChanged : {
                    model.notForPeanuts = outValue;
                }

            }

            /*
            SettingsItem {
                title     : "I work only for polite people."
                helpText  : "I don't read all-caps, 'couse I don't like " +
                "yelling people. If a job contains threatening expressions " +
                "describing how my application will be rejected or even " +
                "just uses too much exclamation marks I don't even want to " +
                "see it."
            }*/
            
            SettingsItem {
                id        : notPhpCowboy
                title     : "No web programming."
                helpText  : "Jobs with PHP, Wordpress and other web " +
                "programming languages and tools will not be shown."
                sensitive : model.filtersEnabled
                inValue   : model.notPhpCowboy
                onOutValueChanged : {
                    model.notPhpCowboy = outValue;
                }
            }
            
            SettingsItem {
                title     : "No grey areas of computing."
                helpText  : "Web scraping, crawling jobs will be filtered " +
                "out, together with proposals that mentions adult content. " +
                "All the SEO jobs will also be filtered."
                sensitive : model.filtersEnabled
                inValue   : model.noGreyComputing
                onOutValueChanged : {
                    model.noGreyComputing = outValue
                }
            }
            
            SettingsItem {
                title     : "No Android, please."
                helpText  : "All the Android programming jobs will be filtered."
                sensitive : model.filtersEnabled
                inValue   : model.noAndroid
                onOutValueChanged : {
                    model.noAndroid = outValue
                }
            }
            
            SettingsItem {
                title     : "No iPhone, please."
                helpText  : "The iPhone and iPad programming jobs will not " +
                "be shown."
                sensitive : model.filtersEnabled
                inValue   : model.noiPhone
                onOutValueChanged : {
                    model.noiPhone = outValue
                }
            }

            Item {
                height: 30
                width: 10
            }
        }
    }

    BottomShadow {
        anchors.top   : parent.top
        //anchors.topMargin: 5
    }
}
