import QtQuick 1.1

XmlListModel {
    id: skills

    source: "http://www.odesk.com/api/profiles/v1/metadata/skills.xml"
    query: "/response/skills/*"
    XmlRole { name: "skill"; query: "string()" }
}

