import QtQuick 1.1
import "theme.js" as Theme

Row {
    id  : root
    anchors.left: parent.left
    anchors.right: parent.right

    property alias title: titleLabel.text
    property alias checked: iSwitch.checked

    signal clicked ();

    Text {
        id             : titleLabel
        verticalAlignment: Text.AlignVCenter
        font.pixelSize : 18
        font.bold      : true
        height         : iSwitch.height
        width: parent.width - iSwitch.width - Theme.normalMargin
    }

    SlideSwitch {
        id  : iSwitch
        checked : model.filtersEnabled
        onClicked : {
            root.clicked()
        }
    }

    MouseArea {
        // this causes a warning...
        anchors.fill: parent
        onClicked : {
            root.clicked ();
        }
    }
}

