import QtQuick 1.1

Rectangle {
    id        : root
    width     : 88
    height    : 35
    state     : checked ? "on" : "off"

    property bool on: false
    property int maxX: background.width - handle.width

    property bool checked: false
    signal clicked ()

    Image {
        id      : background
        height  : parent.height
        width   : parent.width
        source  : "pixmaps/switchbackground.png"

        MouseArea {
            anchors.fill: parent
            onClicked : {
                root.clicked ()
            }
        }
    }

    Image {
        id     : handle
        source : "pixmaps/switchhandle.png"
        height : parent.height
        width  : background.width / 2 + 5

        MouseArea {
            anchors.fill  : parent
            drag.target   : handle
            drag.axis     : Drag.XAxis
            drag.minimumX : 0
            drag.maximumX : root.maxX
            onReleased    : releaseSwitch()
        }
    }

    states: [
        State {
            name: "on"
            PropertyChanges { target: handle; x: root.maxX }
            PropertyChanges { target: root; on: true }
        },
        State {
            name: "off"
            PropertyChanges { target: handle; x: 0 }
            PropertyChanges { target: root; on: false }
        }
    ]

    transitions: Transition {
        NumberAnimation {
            properties: "x";
            easing.type: Easing.InOutQuad;
            duration: 100
        }
    }

    function releaseSwitch() {
        if (handle.x == 1) {
            if (root.state == "off")
                return;
        }

        if (handle.x == 78) {
            if (root.state == "on")
                return;
        }

        root.clicked();
    }
}
