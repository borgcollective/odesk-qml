import QtQuick 1.1
import "theme.js" as Theme
import oDeskBackend 1.0

Rectangle {
    id            : root
    anchors.left  : parent.left
    anchors.right : parent.right
    anchors.top   : parent.top
    color         : "#00aeec"
    height        : 60

    property int nJobs: 0
    property int currentPage: 0

    signal settingsActivated ();
    signal findworkActivated ();

    onCurrentPageChanged: {
        console.log ("currentPage: ", currentPage);
    }

    Text {
        anchors.top     : parent.top
        anchors.left    : parent.left
        anchors.margins : Theme.normalMargin
        visible         : nJobs > 0
        text            : nJobs > 1 ? (nJobs) + " jobs" : "1 job"
        color           : "white"
        smooth          : true
    }

    Text {
        anchors.top     : parent.top
        anchors.right   : parent.right
        anchors.margins : Theme.normalMargin
        //visible         : nJobs > 0
        smooth          : true

        color           : {
            if (backend.sqlStatus == SqlDatasase.SqlStatusConnectError)
                return "Red"

            return "White"
        }

        text : {
            /*return backend.sqlStatus*/

            if (backend.sqlStatus == SqlDatasase.SqlStatusConnectError)
                return "SQL error"
            else if (backend.sqlStatus == SqlDatasase.SqlStatusConnected)
                return "SQL ok"
            else if (backend.sqlStatus == SqlDatasase.SqlStatusConnecting)
                return "SQL connecting"

            return ""
        }
    }

    TopMenuItem {
        anchors.bottom : parent.bottom
        anchors.left   : parent.left
        text           : "Find work"
        sensitive      : currentPage != 0
        onClicked      : root.findworkActivated()
    }

    TopMenuItem {
        id              : quitMenu
        anchors.bottom  : parent.bottom
        anchors.right   : parent.right
        text            : "Exit"

        onClicked : {
            Qt.quit()
        }
    }

    TopMenuItem {
        anchors.bottom : parent.bottom
        anchors.right  : quitMenu.left
        text           : "Settings"
        sensitive      : currentPage != 1
        onClicked      : root.settingsActivated()
    }
}
