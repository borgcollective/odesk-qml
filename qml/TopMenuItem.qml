import QtQuick 1.1
import "theme.js" as Theme

Text {
    id              : root
    anchors.margins : Theme.normalMargin
    color           : "white"
    font.family     : "arial"
    font.pixelSize  : 18
    font.bold       : true
    opacity         : sensitive ? 1.0 : 0.2

    signal clicked ()
    property bool sensitive: true

    MouseArea {
        id           : mouseArea
        anchors.fill : parent

        onClicked : {
            if (root.sensitive)
                root.clicked();
        }
    }
}
