var smallMargin  = 2
var normalMargin = 5
var mediumMargin = 10

var thinBorderWidth   = 1
var normalBorderWidth = 3

var listItemSpacing = 5

var normalRadius = 3

// Animations
var normalAnimationLength = 300

// Fonts
// The font rendering and antialiasing on windows is not very good. The arial
// font seems a little bit smoother than the default.
var fontFamily = "Arial"
var subTitleFontSize  = 18

// Colors
var oDeskBlue   = "#00aeec"
var darkGrey    = "#bdbdbd"
var redHColor   = "Red"
var greenHColor = "#78d948"
var greenLColor = "#5fc62f"
var greenFColor = "#509e19"
var greyHColor  = "White"
var greyLColor  = "#dddddd"
var greyFColor  = "#bbbbbb"
