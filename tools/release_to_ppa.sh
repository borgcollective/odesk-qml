#!/bin/bash
# For changelog entries & signing
export GPGKEY=BDD65CD6
export DEBEMAIL="david.kedves@borgcollective.eu"
export DEBFULLNAME="David Kedves"

if [ ! -f debian/changelog ]; then
    echo "You must call this script from the project rootdir"
    exit 1
fi


# Build precise source package
debchange --distribution precise --team "precise packaging"
dpkg-buildpackage -S -k${GPGKEY}
# Build quantal source package
debchange --distribution quantal --team "quantal packaging"
dpkg-buildpackage -S -k${GPGKEY}
# Build raring source package
debchange --distribution raring --team "raring packaging"
dpkg-buildpackage -S -k${GPGKEY}
# Build saucy source package
debchange --distribution saucy --team "saucy packaging"
dpkg-buildpackage -S -k${GPGKEY}
# Build trusty source package
debchange --distribution trusty --team "trusty packaging"
dpkg-buildpackage -S -k${GPGKEY}

# Upload
cd ..
dput ppa:borgcollective/odesk-qml odesk-qml*.changes
